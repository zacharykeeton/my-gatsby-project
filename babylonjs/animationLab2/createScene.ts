import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";

function setupDoor(scene: BABYLON.Scene, frameRate: number) {
    // SETUP HINGE
    const hinge = BABYLON.MeshBuilder.CreateBox("hinge", {}, scene);
    hinge.position.y = 2;
    hinge.isVisible = false;

    // Animation
    //for hinge to open and close door
    var sweep = new BABYLON.Animation(
        "sweep",
        "rotation.y",
        frameRate,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    sweep.setKeys([
        {
            frame: 0,
            value: 0,
        },
        {
            frame: 3 * frameRate,
            value: 0,
        },
        {
            frame: 5 * frameRate,
            value: Math.PI / 3,
        },
        {
            frame: 13 * frameRate,
            value: Math.PI / 3,
        },
        {
            frame: 15 * frameRate,
            value: 0,
        },
    ]);

    // scene.beginDirectAnimation(hinge, [sweep], 0, 25 * frameRate, false);
    hinge.animations.push(sweep);

    // CREATE DOOR
    const door = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 2, height: 4, depth: 0.1 },
        scene
    );
    door.parent = hinge;
    door.position.x = -1;

    // Start animation
    scene.beginAnimation(hinge, 0, 25 * frameRate, false);
}

function setupSphereLights(scene: BABYLON.Scene) {
    const sphereLight1 = BABYLON.MeshBuilder.CreateSphere(
        "sphere",
        { diameter: 0.2 },
        scene
    );
    sphereLight1.material = new BABYLON.StandardMaterial("", scene);
    (sphereLight1.material as BABYLON.StandardMaterial).emissiveColor =
        BABYLON.Color3.White();
    sphereLight1.position = new BABYLON.Vector3(2, 3, 0.1);

    const sphereLight2 = sphereLight1.clone("");
    sphereLight2.position = new BABYLON.Vector3(-2, 3, 6.9);

    return [sphereLight1, sphereLight2];
}

function setupSpotlights(scene: BABYLON.Scene, frameRate: number) {
    const sphereLights = setupSphereLights(scene);
    const spotlightDirections = [
        new BABYLON.Vector3(-0.5, -0.25, 1),
        new BABYLON.Vector3(0, 0, -1),
    ];

    // LIGHT DIMMER ANIMATION
    //for light to brighten and dim
    const lightDimmer = new BABYLON.Animation(
        "dimmer",
        "intensity",
        frameRate,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    lightDimmer.setKeys([
        {
            frame: 0,
            value: 0,
        },
        {
            // BEGIN FADEIN
            frame: 7 * frameRate,
            value: 0,
        },
        {
            frame: 10 * frameRate,
            value: 1,
        },
        {
            // BEGIN FADEOUT
            frame: 14 * frameRate,
            value: 1,
        },
        {
            frame: 15 * frameRate,
            value: 0,
        },
    ]);

    sphereLights.forEach((sphereLight, i) => {
        const spotlight = new BABYLON.SpotLight(
            `spotlight${i}`, // name
            sphereLight.position, // position
            spotlightDirections[i], // direction
            Math.PI / 8, // angle
            5, // exponent
            scene
        );
        spotlight.diffuse = new BABYLON.Color3(1, 1, 1);
        spotlight.specular = new BABYLON.Color3(0.5, 0.5, 0.5);
        spotlight.intensity = 0;
        spotlight.animations.push(lightDimmer);

        scene.beginAnimation(spotlight, 0, 25 * frameRate, false);
    });
}

function setupCamera(scene: BABYLON.Scene, frameRate: number) {
    const camera = new BABYLON.UniversalCamera(
        "UniversalCamera",
        new BABYLON.Vector3(0, 3, -30),
        scene
    );

    /*********animations*************/
    //for camera to sweep round
    var rotate = new BABYLON.Animation(
        "rotate",
        "rotation.y",
        frameRate,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    var rotate_keys = [];

    rotate_keys.push({
        frame: 0,
        value: 0,
    });

    rotate_keys.push({
        frame: 9 * frameRate,
        value: 0,
    });

    rotate_keys.push({
        frame: 14 * frameRate,
        value: Math.PI,
    });

    rotate.setKeys(rotate_keys);

    //for camera move forward
    var movein = new BABYLON.Animation(
        "movein",
        "position",
        frameRate,
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    var movein_keys = [];

    movein_keys.push({
        frame: 0,
        value: new BABYLON.Vector3(0, 5, -30),
    });

    movein_keys.push({
        frame: 3 * frameRate,
        value: new BABYLON.Vector3(0, 2, -10),
    });

    movein_keys.push({
        frame: 5 * frameRate,
        value: new BABYLON.Vector3(0, 2, -10),
    });

    movein_keys.push({
        frame: 8 * frameRate,
        value: new BABYLON.Vector3(-2, 2, 3),
    });

    movein.setKeys(movein_keys);

    /*******Run Clips**********/

    scene.beginDirectAnimation(
        camera,
        [movein, rotate],
        0,
        25 * frameRate,
        false
    );

    return camera;
}

function createBuilding(scene: BABYLON.Scene) {
    var wall1 = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 8, height: 6, depth: 0.1 },
        scene
    );
    wall1.position.x = -6;
    wall1.position.y = 3;

    var wall2 = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 4, height: 6, depth: 0.1 },
        scene
    );
    wall2.position.x = 2;
    wall2.position.y = 3;

    var wall3 = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 2, height: 2, depth: 0.1 },
        scene
    );
    wall3.position.x = -1;
    wall3.position.y = 5;

    var wall4 = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 14, height: 6, depth: 0.1 },
        scene
    );
    wall4.position.x = -3;
    wall4.position.y = 3;
    wall4.position.z = 7;

    var wall5 = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 7, height: 6, depth: 0.1 },
        scene
    );
    wall5.rotation.y = Math.PI / 2;
    wall5.position.x = -10;
    wall5.position.y = 3;
    wall5.position.z = 3.5;

    var wall6 = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 7, height: 6, depth: 0.1 },
        scene
    );
    wall6.rotation.y = Math.PI / 2;
    wall6.position.x = 4;
    wall6.position.y = 3;
    wall6.position.z = 3.5;

    var roof = BABYLON.MeshBuilder.CreateBox(
        "door",
        { width: 14, height: 7, depth: 0.1 },
        scene
    );
    roof.rotation.x = Math.PI / 2;
    roof.position.x = -3;
    roof.position.y = 6;
    roof.position.z = 3.5;
}

function createScene(engine: BABYLON.Engine) {
    const scene = new BABYLON.Scene(engine);

    // An example of a directional light is when a distant
    // planet is lit by the apparently parallel lines of light from its sun
    const directionalLight = new BABYLON.DirectionalLight(
        "directionalLight",
        new BABYLON.Vector3(0, -1, 0),
        scene
    );
    directionalLight.intensity = 0.25;

    // The HemisphericLight simulates the ambient environment light
    const hemisphericLight = new BABYLON.HemisphericLight(
        "hemisphericLight",
        new BABYLON.Vector3(0, 1, -1),
        scene
    );
    hemisphericLight.intensity = 0.5;

    const frameRate = 20;

    /*********performers*********/
    // camera
    const uCamera = new BABYLON.UniversalCamera(
        "UniversalCamera",
        new BABYLON.Vector3(0, 3, -30),
        scene
    );
    uCamera.attachControl();

    //     /**************Peripherals of Scene***************/
    var ground = BABYLON.MeshBuilder.CreateGround(
        "ground",
        { width: 50, height: 50 },
        scene
    );

    createBuilding(scene);

    // DOOR
    setupDoor(scene, frameRate);

    // SPOTLIGHTS
    setupSpotlights(scene, frameRate);

    // CAMERA
    const movingCamera = setupCamera(scene, frameRate);
    scene.activeCamera = movingCamera;

    return scene;
}

export default createScene;
