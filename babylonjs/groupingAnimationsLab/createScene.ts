import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";

import makeTextPlane from "../shapesDemo/makeTextPlane";

function createCamera(scene: BABYLON.Scene) {
    const camera = new BABYLON.ArcRotateCamera(
        "Camera",
        0,
        0.8,
        100,
        BABYLON.Vector3.Zero(),
        scene
    );
    camera.attachControl(true);
    return camera;
}

function createLight(scene: BABYLON.Scene) {
    // A pointLight is like a little sun.
    // It emits light in every direction
    const pointLight = new BABYLON.PointLight(
        "Omni",
        new BABYLON.Vector3(0, 100, 100),
        scene
    );

    const pointlightSphere = BABYLON.MeshBuilder.CreateSphere(
        "pointlightSphere",
        { diameter: 5 },
        scene
    );
    pointlightSphere.position = pointLight.position;
    const pointlightSphereMaterial = new BABYLON.StandardMaterial(
        "pointlightSphereMat",
        scene
    );
    pointlightSphereMaterial.emissiveColor = BABYLON.Color3.White();
    pointlightSphere.material = pointlightSphereMaterial;

    return pointLight;
}

function createGround(scene: BABYLON.Scene) {
    // GROUND
    // Ground for positional reference
    const ground = BABYLON.MeshBuilder.CreateGround("ground", {
        width: 60,
        height: 60,
    });
    ground.material = new GridMaterial("groundMat", scene);
    ground.material.backFaceCulling = false;
}

function createScalingAnimation(frameRate: number) {
    //Create a scaling animation
    const scalingAnimation = new BABYLON.Animation(
        "scalingAnimation",
        "scaling.z",
        frameRate,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
    );

    scalingAnimation.setKeys([
        {
            frame: 0,
            value: 1,
        },
        {
            frame: 20,
            value: 0.2,
        },
        {
            frame: 100,
            value: 1,
        },
    ]);

    return scalingAnimation;
}

function createRotationAnimation(frameRate: number) {
    //Create a second rotation animation with different timeline
    const rotationAnimation = new BABYLON.Animation(
        "rotationAnimation",
        "rotation.y",
        frameRate,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
    );
    rotationAnimation.setKeys([
        {
            frame: 0,
            value: 0,
        },

        {
            frame: 40,
            value: Math.PI,
        },

        {
            frame: 80,
            value: 0,
        },
    ]);

    return rotationAnimation;
}

function createBox1(scene: BABYLON.Scene) {
    const box1 = BABYLON.MeshBuilder.CreateBox("Box1", { size: 10 }, scene);
    box1.position.x = -20;
    box1.position.y = 6;
    const materialBox1 = new BABYLON.StandardMaterial("texture1", scene);
    materialBox1.diffuseColor = BABYLON.Color3.Green();
    box1.material = materialBox1;
    return box1;
}

function createBox1Clone(
    scene: BABYLON.Scene,
    animations: BABYLON.Animation[]
) {
    const text = makeTextPlane("not normalized", "white", 100, scene);
    text.rotation.x = Math.PI / 2;
    text.rotation.y = -Math.PI / 2;
    text.position.z = 20;
    text.position.x = 9.5;
    text.position.y = 0.1;
    const box1Clone = BABYLON.MeshBuilder.CreateBox(
        "box1Clone",
        { size: 10 },
        scene
    );
    box1Clone.position.z = 20;
    box1Clone.position.y = 6;

    const materialBox1Clone = new BABYLON.StandardMaterial("texture1", scene);
    materialBox1Clone.diffuseColor = BABYLON.Color3.Red();
    box1Clone.material = materialBox1Clone;
    // play animations
    scene.beginDirectAnimation(box1Clone, animations, 0, 100, true);
    return box1Clone;
}

function createBox2(scene: BABYLON.Scene) {
    const box2 = BABYLON.MeshBuilder.CreateBox("Box2", { size: 10 }, scene);
    box2.position.x = 20;
    box2.position.y = 6;
    const materialBox2 = new BABYLON.StandardMaterial("texture2", scene);
    box2.material = materialBox2;
    return box2;
}

function createAnimationGroup(
    box1: BABYLON.Mesh,
    box2: BABYLON.Mesh,
    scalingAnimation: BABYLON.Animation,
    rotationAnimation: BABYLON.Animation
) {
    // Create the animation group
    const animationGroup = new BABYLON.AnimationGroup("my group");
    animationGroup.addTargetedAnimation(scalingAnimation.clone(), box1);
    animationGroup.addTargetedAnimation(rotationAnimation.clone(), box1);
    animationGroup.addTargetedAnimation(rotationAnimation.clone(), box2);

    // Make sure to normalize animations to the same timeline
    animationGroup.normalize(0, 100);
    // animationGroup.play(true);

    animationGroup.speedRatio = 2;
    return animationGroup;
}

function createScene(engine: BABYLON.Engine) {
    const scene = new BABYLON.Scene(engine);

    createCamera(scene);
    createLight(scene);
    createGround(scene);

    const frameRate = 30;
    const scalingAnimation = createScalingAnimation(frameRate);
    const rotationAnimation = createRotationAnimation(frameRate);

    //Boxes
    const box1 = createBox1(scene);
    createBox1Clone(scene, [scalingAnimation, rotationAnimation]);
    const box2 = createBox2(scene);

    // ANIMATION GROUP
    const animationGroup = createAnimationGroup(
        box1,
        box2,
        scalingAnimation,
        rotationAnimation
    );

    // triggers on .stop or if animation doesn't loop
    animationGroup.onAnimationEndObservable.add(function () {
        console.log("animationEnd");
    });

    // // UI
    // Creates a new AdvancedDynamicTexture in fullscreen mode
    const advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    const panel = new GUI.StackPanel("guiStackPanel");
    panel.isVertical = false;
    panel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
    advancedTexture.addControl(panel);
    panel.left = 0;
    panel.top = 10;
    // panel.rotation = 1;
    panel.height = "40px";

    var text1 = new GUI.TextBlock();
    text1.text = "Animation Group";
    text1.color = "white";
    text1.fontSize = 24;
    panel.addControl(text1);
    text1.width = "200px";

    const addButton = function (text: string, callback: Function) {
        const button = GUI.Button.CreateSimpleButton("button", text);
        button.width = "140px";
        button.height = "40px";
        button.color = "white";
        button.background = "green";
        button.paddingLeft = "10px";
        button.paddingRight = "10px";
        button.onPointerUpObservable.add(function () {
            callback();
        });
        panel.addControl(button);
    };

    addButton("Play", function () {
        animationGroup.play(true);
    });

    addButton("Pause", function () {
        animationGroup.pause();
    });

    addButton("Reset", function () {
        animationGroup.reset();
        animationGroup.stop();
    });

    return scene;
}

export default createScene;
