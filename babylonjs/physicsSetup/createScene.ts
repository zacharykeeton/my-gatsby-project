import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";
import { PhysicsEngine, HavokPlugin } from "@babylonjs/core/Physics";
import HavokPhysics from "@babylonjs/havok";

import makeTextPlane from "../shapesDemo/makeTextPlane";
import { SceneExplorerComponent } from "@babylonjs/inspector/components/sceneExplorer/sceneExplorerComponent";

function createCamera(scene: BABYLON.Scene) {
    // This creates and positions a free camera (non-mesh)
    const camera = new BABYLON.FreeCamera(
        "camera1",
        new BABYLON.Vector3(0, 5, -10),
        scene
    );

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(true);
}

function createLight(scene: BABYLON.Scene) {
    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    const light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;
}

function createSphere(scene: BABYLON.Scene) {
    // Our built-in 'sphere' shape.
    const sphere = BABYLON.MeshBuilder.CreateSphere(
        "sphere",
        { diameter: 2, segments: 32 },
        scene
    );

    // Move the sphere upward at 4 units
    sphere.position.y = 4;
    return sphere;
}

function createGround(scene: BABYLON.Scene) {
    // Our built-in 'ground' shape.
    const ground = BABYLON.MeshBuilder.CreateGround(
        "ground",
        { width: 10, height: 10 },
        scene
    );
    return ground;
}

async function createScene(engine: BABYLON.Engine) {
    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);

    createCamera(scene);
    createLight(scene);
    const sphere = createSphere(scene);
    const ground = createGround(scene);
    // ground.rotation.z = Math.PI / 4;

    // initialize plugin
    const havokInterface = await HavokPhysics();
    var hk = new HavokPlugin(true, havokInterface);
    // enable physics in the scene with a gravity
    const gravity = new BABYLON.Vector3(0, -9.8, 0);
    scene.enablePhysics(gravity, hk);

    // Create a sphere shape and the associated body. Size will be determined automatically.
    const sphereAggregate = new BABYLON.PhysicsAggregate(
        sphere,
        BABYLON.PhysicsShapeType.SPHERE,
        { mass: 1, restitution: 0.75 },
        scene
    );

    // Create a static box shape.
    const groundAggregate = new BABYLON.PhysicsAggregate(
        ground,
        BABYLON.PhysicsShapeType.BOX,
        { mass: 0 },
        scene
    );

    return scene;
}

export default createScene;
