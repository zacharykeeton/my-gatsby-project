import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";
import { PhysicsEngine, HavokPlugin } from "@babylonjs/core/Physics";
import HavokPhysics from "@babylonjs/havok";

import makeTextPlane from "../shapesDemo/makeTextPlane";
import { SceneExplorerComponent } from "@babylonjs/inspector/components/sceneExplorer/sceneExplorerComponent";

function createCamera(scene: BABYLON.Scene) {
    // This creates and positions a free camera (non-mesh)
    const camera = new BABYLON.FreeCamera(
        "camera1",
        new BABYLON.Vector3(0, 3, -15),
        scene
    );

    // This targets the camera to scene origin
    camera.setTarget(new BABYLON.Vector3(0, 3, 0));

    // This attaches the camera to the canvas
    camera.attachControl(true);
}

function createLight(scene: BABYLON.Scene) {
    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    const light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.9;
}

function createSphere(scene: BABYLON.Scene) {
    // Our built-in 'sphere' shape.
    const sphere = BABYLON.MeshBuilder.CreateSphere(
        "sphere",
        { diameter: 2, segments: 32 },
        scene
    );

    // Move the sphere upward at 4 units
    sphere.position.y = 4;
    return sphere;
}

function createGround(scene: BABYLON.Scene) {
    // Our built-in 'ground' shape.
    const ground = BABYLON.MeshBuilder.CreateGround(
        "ground",
        { width: 10, height: 10 },
        scene
    );
    return ground;
}

function createLabel(
    scene: BABYLON.Scene,
    position: BABYLON.Vector3,
    text: string
) {
    const dynamicTexture = new BABYLON.DynamicTexture(
        "dynamicTexture" + text,
        512,
        scene,
        true
    );
    dynamicTexture.hasAlpha = true;
    dynamicTexture.drawText(
        text,
        null,
        null,
        "32px Arial",
        "white",
        "transparent"
    );

    var plane = BABYLON.MeshBuilder.CreatePlane(
        "label" + text,
        { size: 2 },
        scene
    );
    plane.scaling.scaleInPlace(3);
    plane.position.copyFrom(position);
    plane.position.y += 2.5;
    plane.position.x += 1.4;
    plane.rotation.z += 1;
    plane.material = new BABYLON.PBRMaterial("material" + text, scene);
    (plane.material as BABYLON.PBRMaterial).unlit = true;
    (plane.material as BABYLON.PBRMaterial).backFaceCulling = false;
    (plane.material as BABYLON.PBRMaterial).albedoTexture = dynamicTexture;
    (plane.material as BABYLON.PBRMaterial).useAlphaFromAlbedoTexture = true;
}

function createBoxes(
    size: number,
    numBoxes: number,
    startAsleep: boolean,
    pos: BABYLON.Vector3,
    yOffset: number,
    scene: BABYLON.Scene,
    restitution: number = 0.5
) {
    // create a series of boxes with phyics aggregates
    for (let i = 0; i < numBoxes; i++) {
        // create box with standard material
        const box = BABYLON.MeshBuilder.CreateBox("box", { size }, scene);
        box.material = new BABYLON.StandardMaterial("boxMat", scene);

        // give box a random color
        (box.material as BABYLON.StandardMaterial).diffuseColor =
            BABYLON.Color3.Random();

        // Clone the given position and
        // adjust given the yOffset
        box.position = pos.clone();
        box.position.y += i * (yOffset + size) + 0.5;

        new BABYLON.PhysicsAggregate(
            box,
            BABYLON.PhysicsShapeType.BOX,
            { mass: 1, startAsleep, restitution },
            scene
        );
    }
}

async function createScene(engine: BABYLON.Engine) {
    // This creates a basic Babylon Scene object (non-mesh)
    const scene = new BABYLON.Scene(engine);
    createCamera(scene);
    createLight(scene);

    // initialize plugin & enable physics in the scene with a gravity
    const havokInterface = await HavokPhysics();
    const hk = new HavokPlugin(undefined, havokInterface);
    const gravity = new BABYLON.Vector3(0, -9.8, 0);
    scene.enablePhysics(gravity, hk);

    // Create a static box shape.
    const ground = createGround(scene);
    const groundAggregate = new BABYLON.PhysicsAggregate(
        ground,
        BABYLON.PhysicsShapeType.BOX,
        { mass: 0 },
        scene
    );

    const boxesSize = 1;
    const boxesHeight = 3;
    const yOffset = 0.5;

    // Create boxes that start in sleep mode
    createBoxes(
        boxesSize,
        boxesHeight,
        true,
        new BABYLON.Vector3(-2, 0, 0),
        yOffset,
        scene
    );
    createLabel(scene, new BABYLON.Vector3(-3, 3, 0), "Start asleep");

    // Create boxes that start awake
    createBoxes(
        boxesSize,
        boxesHeight,
        false,
        new BABYLON.Vector3(2, 0, 0),
        yOffset,
        scene
    );
    createLabel(scene, new BABYLON.Vector3(1, 3, 0), "Start awake");

    // Create button
    const btn = GUI.Button.CreateSimpleButton("btn", "Drop object on towers");
    btn.widthInPixels = 300;
    btn.heightInPixels = 40;
    btn.background = "green";
    btn.color = "white";
    btn.top = "-40%";

    // Add button to screen
    const ui = GUI.AdvancedDynamicTexture.CreateFullscreenUI("ui");
    ui.addControl(btn);

    btn.onPointerClickObservable.add(() => {
        createBoxes(0.2, 1, false, new BABYLON.Vector3(-2, 5, 0), 0, scene);
        createBoxes(0.2, 1, false, new BABYLON.Vector3(2, 5, 0), 0, scene);
    });

    // createCamera(scene);
    // createLight(scene);
    // const sphere = createSphere(scene);
    // const ground = createGround(scene);
    // // ground.rotation.z = Math.PI / 4;

    // // initialize plugin
    // const havokInterface = await HavokPhysics();
    // var hk = new HavokPlugin(true, havokInterface);
    // // enable physics in the scene with a gravity
    // const gravity = new BABYLON.Vector3(0, -9.8, 0);
    // scene.enablePhysics(gravity, hk);

    // // Create a sphere shape and the associated body. Size will be determined automatically.
    // const sphereAggregate = new BABYLON.PhysicsAggregate(
    //     sphere,
    //     BABYLON.PhysicsShapeType.SPHERE,
    //     { mass: 1, restitution: 0.75 },
    //     scene
    // );

    // // Create a static box shape.
    // const groundAggregate = new BABYLON.PhysicsAggregate(
    //     ground,
    //     BABYLON.PhysicsShapeType.BOX,
    //     { mass: 0 },
    //     scene
    // );

    return scene;
}

export default createScene;
