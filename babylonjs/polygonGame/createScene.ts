import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";

import makeTextPlane from "../shapesDemo/makeTextPlane";

function createPoseControls() {
    // // UI
    // var advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    // var UiPanel = new GUI.StackPanel();
    // UiPanel.width = "220px";
    // UiPanel.fontSize = "14px";
    // UiPanel.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
    // UiPanel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_CENTER;
    // advancedTexture.addControl(UiPanel);
    // UiPanel.background = "blue";
    // // Keep track of the current override animation
    // let currentParam = idleParam;
    // function onBeforeAnimation() {
    //     // Increment the weight of the current override animation
    //     if (currentParam) {
    //         currentParam.weight = BABYLON.Scalar.Clamp(
    //             currentParam.weight + 0.01,
    //             0,
    //             1
    //         );
    //         currentParam.anim.weight = currentParam.weight;
    //     }
    //     // Decrement the weight of all override animations that aren't current
    //     if (currentParam !== idleParam) {
    //         idleParam.weight = BABYLON.Scalar.Clamp(
    //             idleParam.weight - 0.01,
    //             0,
    //             1
    //         );
    //         idleParam.anim.weight = idleParam.weight;
    //     }
    //     if (currentParam !== walkParam) {
    //         walkParam.weight = BABYLON.Scalar.Clamp(
    //             walkParam.weight - 0.01,
    //             0,
    //             1
    //         );
    //         walkParam.anim.weight = walkParam.weight;
    //     }
    //     if (currentParam !== runParam) {
    //         runParam.weight = BABYLON.Scalar.Clamp(
    //             runParam.weight - 0.01,
    //             0,
    //             1
    //         );
    //         runParam.anim.weight = runParam.weight;
    //     }
    //     // Remove the callback the current animation weight reaches 1 or
    //     // when all override animations reach 0 when current is undefined
    //     if (
    //         (currentParam && currentParam.weight === 1) ||
    //         (idleParam.weight === 0 &&
    //             walkParam.weight === 0 &&
    //             runParam.weight === 0)
    //     ) {
    //         scene.onBeforeAnimationsObservable.removeCallback(
    //             onBeforeAnimation
    //         );
    //     }
    // }
    // // Button for blending to bind pose
    // var button = GUI.Button.CreateSimpleButton("but0", "None");
    // button.paddingTop = "10px";
    // button.width = "100px";
    // button.height = "50px";
    // button.color = "white";
    // button.background = "green";
    // button.onPointerDownObservable.add(function () {
    //     // Remove current animation
    //     currentParam = undefined;
    //     // Restart animation observer
    //     scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    //     scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
    // });
    // UiPanel.addControl(button);
    // // Button for blending to idle
    // var button = GUI.Button.CreateSimpleButton("but0", "Idle");
    // button.paddingTop = "10px";
    // button.width = "100px";
    // button.height = "50px";
    // button.color = "white";
    // button.background = "green";
    // button.onPointerDownObservable.add(function () {
    //     // Do nothing if idle is already the current animation
    //     if (currentParam === idleParam) {
    //         return;
    //     }
    //     // Restart animation observer with idle set to current
    //     scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    //     currentParam = idleParam;
    //     scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
    // });
    // UiPanel.addControl(button);
    // // Button for blending to walk
    // button = GUI.Button.CreateSimpleButton("but0", "Walk");
    // button.paddingTop = "10px";
    // button.width = "100px";
    // button.height = "50px";
    // button.color = "white";
    // button.background = "green";
    // button.onPointerDownObservable.add(function () {
    //     // Do nothing if walk is already the current animation
    //     if (currentParam === walkParam) {
    //         return;
    //     }
    //     // Synchronize animations
    //     if (currentParam) {
    //         walkParam.anim.syncAllAnimationsWith(null);
    //         currentParam.anim.syncAllAnimationsWith(
    //             walkParam.anim.animatables[0]
    //         );
    //     }
    //     // Restart animation observer with walk set to current
    //     scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    //     currentParam = walkParam;
    //     scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
    // });
    // UiPanel.addControl(button);
    // // Button for blending to run
    // button = GUI.Button.CreateSimpleButton("but0", "Run");
    // button.paddingTop = "10px";
    // button.width = "100px";
    // button.height = "50px";
    // button.color = "white";
    // button.background = "green";
    // button.onPointerDownObservable.add(function () {
    //     // Do nothing if run is already the current animation
    //     if (currentParam === runParam) {
    //         return;
    //     }
    //     // Synchronize animations
    //     if (currentParam) {
    //         runParam.anim.syncAllAnimationsWith(null);
    //         currentParam.anim.syncAllAnimationsWith(
    //             runParam.anim.animatables[0]
    //         );
    //     }
    //     // Restart animation observer with run set to current
    //     scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    //     currentParam = runParam;
    //     scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
    // });
    // UiPanel.addControl(button);
    // // Create a slider to control the weight of each additive pose/animation
    // var params = [sadPoseParam, sneakPoseParam, headShakeParam, agreeParam];
    // params.forEach((param) => {
    //     // Label
    //     var header = new GUI.TextBlock();
    //     header.text = param.name + ":" + param.weight.toFixed(2);
    //     header.height = "40px";
    //     header.color = "green";
    //     header.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    //     header.paddingTop = "10px";
    //     UiPanel.addControl(header);
    //     // Slider
    //     var slider = new GUI.Slider();
    //     slider.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    //     slider.minimum = 0;
    //     slider.maximum = 1;
    //     slider.color = "green";
    //     slider.value = param.anim.weight;
    //     slider.height = "20px";
    //     slider.width = "205px";
    //     UiPanel.addControl(slider);
    //     // Update animation weight value according to slider value
    //     slider.onValueChangedObservable.add((v) => {
    //         param.anim.weight = v;
    //         param.weight = v;
    //         header.text = param.name + ":" + param.weight.toFixed(2);
    //     });
    //     param.anim._slider = slider;
    //     slider.value = param.weight;
    // });
}

function createGround(scene: BABYLON.Scene) {
    // GROUND
    // Ground for positional reference
    const ground = BABYLON.MeshBuilder.CreateGround("ground", {
        width: 60,
        height: 60,
    });
    ground.material = new GridMaterial("groundMat", scene);
    ground.material.backFaceCulling = false;
}

function createCamera(scene: BABYLON.Scene) {
    const camera = new BABYLON.ArcRotateCamera(
        "camera1",
        Math.PI / 2,
        Math.PI / 4,
        3,
        new BABYLON.Vector3(0, 1, 0),
        scene
    );
    camera.attachControl(true);
    camera.lowerRadiusLimit = 2;
    // camera.upperRadiusLimit = 10;
    camera.wheelDeltaPercentage = 0.01;
}

function createHemisphericLight(scene: BABYLON.Scene) {
    const light = new BABYLON.HemisphericLight(
        "light1",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );
    light.intensity = 0.6;
    light.specular = BABYLON.Color3.Black();
}

function createDirectionalLight(scene: BABYLON.Scene) {
    const light2 = new BABYLON.DirectionalLight(
        "dir01",
        new BABYLON.Vector3(0, -0.5, -1.0),
        scene
    );
    light2.position = new BABYLON.Vector3(0, 5, 5);
    return light2;
}
function createEnvironment(scene: BABYLON.Scene) {
    // TODO: learn about skybox, ground
    const createDefaultEnvironmentHelper = scene.createDefaultEnvironment({
        enableGroundShadow: true,
    })!;
    createDefaultEnvironmentHelper.setMainColor(BABYLON.Color3.Gray());
    if (createDefaultEnvironmentHelper.ground?.position) {
        createDefaultEnvironmentHelper.ground.position.y += 0.01;
    }
}

async function importCharacterModel(scene: BABYLON.Scene) {
    // Model by Mixamo
    const { meshes, skeletons, particleSystems } =
        await BABYLON.SceneLoader.ImportMeshAsync(
            "",
            "./",
            "Xbot.glb", // note: needed to import loader
            scene
        );

    // //console.log(meshes);
    const xbot = meshes[0];
    return { xbot, meshes };
}

function setupCharacterMovement(
    scene: BABYLON.Scene,
    xbot: BABYLON.AbstractMesh
) {
    // Initialize override animations, turn on idle by default
    const idleAnim = scene.animationGroups.find((a) => a.name === "idle")!;
    const idleParam = { name: "Idle", anim: idleAnim, weight: 1 };
    idleAnim.weight = 1;
    idleAnim.play(true);

    const walkAnim = scene.animationGroups.find((a) => a.name === "walk")!;
    const walkParam = { name: "Walk", anim: walkAnim, weight: 0 };
    walkAnim.weight = 0;
    walkAnim.play(true);

    const runAnim = scene.animationGroups.find((a) => a.name === "run")!;
    const runParam = { name: "Run", anim: runAnim, weight: 0 };
    runAnim.weight = 0;
    runAnim.play(true);

    // Initialize additive poses. Slice of reference pose at first frame

    // SAD POSE
    const sadPoseAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
        scene.animationGroups.find((a) => a.name === "sad_pose")!
    );
    const sadPoseParam = {
        name: "Sad Pose",
        anim: sadPoseAnim,
        weight: 0,
    };
    sadPoseAnim.weight = 0;
    sadPoseAnim.start(
        true,
        1,
        2.0000001043081284, //0.03333333507180214 * 60,
        2.0000001043081284 //0.03333333507180214 * 60
    );

    // SNEAK POSE
    const sneakPoseAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
        scene.animationGroups.find((a) => a.name === "sneak_pose")!
    );
    const sneakPoseParam = {
        name: "Sneak Pose",
        anim: sneakPoseAnim,
        weight: 0,
    };
    sneakPoseAnim.weight = 0;
    sneakPoseAnim.start(
        true,
        1,
        0.03333333507180214 * 60,
        0.03333333507180214 * 60
    );

    // Initialize additive animations
    const headShakeAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
        scene.animationGroups.find((a) => a.name === "headShake")!
    );
    const headShakeParam = {
        name: "Head Shake",
        anim: headShakeAnim,
        weight: 0,
    };
    headShakeAnim.weight = 0;
    headShakeAnim.play(true);

    const agreeAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
        scene.animationGroups.find((a) => a.name === "agree")!
    );
    const agreeParam = { name: "Agree", anim: agreeAnim, weight: 0 };
    agreeAnim.weight = 0;
    agreeAnim.play(true);

    // CAPTURE KEYPRESSES
    // Keyboard events
    let keydowns = 0;
    const inputMap: any = {};
    scene.actionManager = new BABYLON.ActionManager(scene);
    scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(
            BABYLON.ActionManager.OnKeyDownTrigger,
            function (evt) {
                //console.log(evt.sourceEvent.type, ++keydowns);
                inputMap[evt.sourceEvent.key] =
                    evt.sourceEvent.type == "keydown";

                //console.log(inputMap);
            }
        )
    );
    scene.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(
            BABYLON.ActionManager.OnKeyUpTrigger,
            function (evt) {
                //console.log(
                evt.sourceEvent.key,
                    evt.sourceEvent.key.toUpperCase(),
                    evt.sourceEvent.key.toLowerCase();
                // );

                inputMap[evt.sourceEvent.key] =
                    evt.sourceEvent.type == "keydown";

                inputMap[evt.sourceEvent.key.toUpperCase()] =
                    evt.sourceEvent.type == "keydown";

                inputMap[evt.sourceEvent.key.toLowerCase()] =
                    evt.sourceEvent.type == "keydown";
            }
        )
    );

    // MOVEMENT
    let hero = xbot;
    let characterIsIdle = true;
    const heroWalkSpeed = 0.028;
    const heroRunSpeed = heroWalkSpeed * 2;
    let heroSpeed = heroWalkSpeed;
    const heroSpeedBackwards = 0.01;
    const heroRotationSpeed = 0.1;
    //Rendering loop (executed for everyframe)
    scene.onBeforeRenderObservable.add(() => {
        // if (xbot.intersectsMesh(leftBox)) {
        //     //console.log("TOUCHING");
        // }
        let keydown = false; // helps with animations

        // MOVEMENTS

        // WALKING
        // WALKING FORWARD
        if (inputMap["w"]) {
            console.log("walk forward");
            hero.moveWithCollisions(hero.forward.scaleInPlace(heroWalkSpeed));
            keydown = true;
        }

        // WALKING BACKWARD
        if (inputMap["s"]) {
            //console.log("walk backward");

            hero.moveWithCollisions(
                hero.forward.scaleInPlace(-heroSpeedBackwards)
            );
            keydown = true;
        }

        // WALKING TURN LEFT
        if (inputMap["a"]) {
            hero.rotate(BABYLON.Vector3.Up(), -heroRotationSpeed);
            keydown = true;
            // //console.log("TURN LEFT");
        }

        // WALKING TURN RIGHT
        if (inputMap["d"]) {
            // //console.log("TURN RIGHTT");
            hero.rotate(BABYLON.Vector3.Up(), heroRotationSpeed);
            keydown = true;
        }

        // RUNNING
        // // RUNNING FORWARD
        if (inputMap["Shift"] && (inputMap["W"] || inputMap["w"])) {
            hero.moveWithCollisions(hero.forward.scaleInPlace(heroRunSpeed));
            keydown = true;
        }

        // RUNNING BACKWARD
        if (inputMap["Shift"] && inputMap["S"]) {
            hero.moveWithCollisions(
                hero.forward.scaleInPlace(-heroSpeedBackwards)
            );
            keydown = true;
        }

        // RUNNING TURN LEFT
        if (inputMap["Shift"] && inputMap["A"]) {
            hero.rotate(BABYLON.Vector3.Up(), -heroRotationSpeed);
            keydown = true;
            // //console.log("TURN LEFT");
        }

        // RUNNING TURN RIGHT
        if (inputMap["Shift"] && inputMap["D"]) {
            // //console.log("TURN RIGHTT");
            hero.rotate(BABYLON.Vector3.Up(), heroRotationSpeed);
            keydown = true;
        }

        // ANIMATIONS

        if (keydown) {
            // if keydown and character was idle
            // start an animation
            if (characterIsIdle) {
                // Stop being idle
                idleAnim.weight = 0;
                characterIsIdle = false;

                // Start walking animation
                // walkAnim.weight = 1;
                walkAnim.weight = 1;
            } else {
                if (inputMap["Shift"] && (inputMap["W"] || inputMap["w"])) {
                    walkAnim.weight = 0;
                    runAnim.weight = 1;
                    //console.log("RUNNNNN");
                } else {
                    runAnim.weight = 0;
                    walkAnim.weight = 1;
                }
            }
        } else {
            // if key is not pressed and
            // character is not idle
            // become idle
            if (!characterIsIdle) {
                // stop other animations
                walkAnim.weight = 0;
                runAnim.weight = 0;

                // become idle
                idleAnim.weight = 1;
                characterIsIdle = true;
            }
        }
    });
}
function createGUI(scene: BABYLON.Scene) {
    const advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

    const UiPanel = new GUI.StackPanel();
    UiPanel.width = "220px";
    UiPanel.fontSize = "14px";
    UiPanel.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
    UiPanel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
    advancedTexture.addControl(UiPanel);
    UiPanel.background = "blue";

    const textBox = new GUI.TextBlock("promptText", "left");
    textBox.height = "40px";
    textBox.color = "white";
    textBox.fontSize = "40px";
    UiPanel.addControl(textBox);
    return textBox;
}
async function createScene(engine: BABYLON.Engine) {
    // engine.enableOfflineSupport = false;
    // engine.displayLoadingUI();
    var scene = new BABYLON.Scene(engine);

    // Camera
    createCamera(scene);

    // Lights
    createHemisphericLight(scene);
    const light2 = createDirectionalLight(scene);

    // Environment
    createEnvironment(scene);

    // GUI

    const promptTextBox = createGUI(scene);

    // IMPORT XBOT MODEL
    const { xbot, meshes } = await importCharacterModel(scene);

    // xbot.getChildMeshes().forEach((childMesh) => (childMesh.visibility = 1));
    // xbot.checkCollisions = true;

    const leftBox = BABYLON.MeshBuilder.CreateBox("box", {}, scene);
    leftBox.position = new BABYLON.Vector3(2, 0.5, 0);
    leftBox.material = new BABYLON.StandardMaterial("leftBoxBoxMat", scene);
    // box.checkCollisions = true;
    (leftBox.material as BABYLON.StandardMaterial).diffuseColor =
        BABYLON.Color3.Red();

    const centerBox = BABYLON.MeshBuilder.CreateBox("box1", {}, scene);
    centerBox.position = new BABYLON.Vector3(0, 0.5, 0);
    centerBox.material = new BABYLON.StandardMaterial("centerBoxBoxMat", scene);
    (centerBox.material as BABYLON.StandardMaterial).diffuseColor =
        BABYLON.Color3.Green();

    const rightBox = BABYLON.MeshBuilder.CreateBox("box2", {}, scene);
    rightBox.position = new BABYLON.Vector3(-2, 0.5, 0);
    rightBox.material = new BABYLON.StandardMaterial("rightBoxBoxMat", scene);
    (rightBox.material as BABYLON.StandardMaterial).diffuseColor =
        BABYLON.Color3.Blue();

    // setup game state
    interface GameState {
        gameOver: Boolean;
        playerWins: Boolean;
        selectedChoice: BABYLON.Mesh;
        problems: {
            left: { prompt: string; answer: BABYLON.Mesh };
            right: { prompt: string; answer: BABYLON.Mesh };
            center: { prompt: string; answer: BABYLON.Mesh };
        };
        currentProblem: { prompt: string; answer: BABYLON.Mesh } | undefined;
    }
    const gameState: GameState = {
        gameOver: false,
        playerWins: false,
        selectedChoice: leftBox,
        problems: {
            left: {
                prompt: "left",
                answer: leftBox,
            },
            right: {
                prompt: "right",
                answer: rightBox,
            },
            center: {
                prompt: "center",
                answer: centerBox,
            },
        },
        currentProblem: undefined,
    };
    gameState.currentProblem = gameState.problems.left;
    function pickRandomProblem() {
        const random = Math.floor(Math.random() * 3 + 1);
        switch (random) {
            case 1:
                return gameState.problems.left;
                break;
            case 2:
                return gameState.problems.center;
                break;
            case 3:
                return gameState.problems.right;
                break;
            default:
                return gameState.problems.left;
                break;
        }
    }

    let problemSetup = false;
    scene.onBeforeRenderObservable.add(() => {
        if (!problemSetup) {
            // pick new problem
            gameState.currentProblem = pickRandomProblem();
            promptTextBox.text = gameState.currentProblem.prompt;

            problemSetup = true;
        } else if (gameState.currentProblem) {
            if (xbot.intersectsMesh(gameState.currentProblem.answer)) {
                console.log("CORRECT");
            }
        }
    });

    // Shadows
    const shadowGenerator = new BABYLON.ShadowGenerator(1024, light2);
    shadowGenerator.useBlurExponentialShadowMap = true;
    shadowGenerator.blurKernel = 32;
    shadowGenerator.addShadowCaster(meshes[0], true);
    shadowGenerator.addShadowCaster(leftBox, true);
    shadowGenerator.addShadowCaster(centerBox, true);
    shadowGenerator.addShadowCaster(rightBox, true);
    for (var index = 0; index < meshes.length; index++) {
        meshes[index].receiveShadows = false;
    }

    setupCharacterMovement(scene, xbot);

    engine.hideLoadingUI();
    return scene;
}

export default createScene;
