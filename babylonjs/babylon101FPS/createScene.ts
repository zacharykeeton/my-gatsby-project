// TEXTBOOK
// https://research.ncl.ac.uk/game/mastersdegree/graphicsforgames/

import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";
import { PhysicsEngine, HavokPlugin } from "@babylonjs/core/Physics";
import HavokPhysics from "@babylonjs/havok";

import makeTextPlane from "../shapesDemo/makeTextPlane";

var mapSize = 80;
var mouseSensitivity = 0.0002;

function mouseMove(
    evt: MouseEvent,
    engine: BABYLON.Engine,
    playerMesh: BABYLON.AbstractMesh
) {
    const deltaTime = engine.getDeltaTime();
    playerMesh.rotation.x += evt.movementY * deltaTime * mouseSensitivity;
    playerMesh.rotation.y -= evt.movementX * deltaTime * mouseSensitivity;
}

function setupPointerLock(
    engine: BABYLON.Engine,
    playerMesh: BABYLON.AbstractMesh
) {
    // when element is clicked, we're going to request a
    // pointerlock
    const canvas = document.querySelector(
        "#babylonCanvas"
    ) as HTMLCanvasElement;
    canvas.onclick = function () {
        // Ask the browser to lock the pointer)
        canvas.requestPointerLock();
    };

    const mouseMoveCallback = (evt: MouseEvent) => {
        console.log("MOUSE MOVE");
        mouseMove(evt, engine, playerMesh);
    };

    // register the callback when a pointerlock event occurs
    let pointerAlreadyLocked = false;
    document.addEventListener(
        "pointerlockchange",
        (evt) => {
            console.log("pointerlockchange");

            if (document.pointerLockElement === canvas) {
                if (pointerAlreadyLocked) {
                    console.log("POINTER ALREADY LOCKED");
                    return;
                }
                document.addEventListener(
                    "mousemove",
                    mouseMoveCallback,
                    false
                );
                pointerAlreadyLocked = true;
                console.log("POINTER NOW LOCKED");
            } else {
                console.log("POINTER UNLOCKED");
                document.removeEventListener(
                    "mousemove",
                    mouseMoveCallback,
                    false
                );
                pointerAlreadyLocked = false;
            }
        },
        false
    );
}

function setupScene(engine: BABYLON.Engine) {
    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);
    scene.useRightHandedSystem = true;

    const assumedFramesPerSecond = 60;
    const earthGravity = -9.81;
    scene.gravity = new BABYLON.Vector3(
        0,
        earthGravity / assumedFramesPerSecond,
        0
    );

    ////// COLLISIONS STEP 3 of 3: Apply collision
    // Enable collisions in scene
    scene.collisionsEnabled = true;

    return scene;
}

function createCamera(scene: BABYLON.Scene) {
    // First person camera
    const fpsCamera = new BABYLON.UniversalCamera(
        "camera",
        new BABYLON.Vector3(0, 10, 0),
        scene
    );
    return fpsCamera;
}

function createLight(scene: BABYLON.Scene) {
    const light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );
    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;
}

function createThing(scene: BABYLON.Scene) {}

const createScene = async function (engine: BABYLON.Engine) {
    // ENGINE
    engine.enableOfflineSupport = false;
    engine.displayLoadingUI();

    // SCENE
    const scene = setupScene(engine);
    const framesPerSecond = 60;
    const gravity = -9.81;
    scene.gravity = new BABYLON.Vector3(0, gravity / framesPerSecond, 0);
    scene.collisionsEnabled = true;

    // LIGHT
    createLight(scene);

    // CAMERA
    const fpsCamera = createCamera(scene);
    fpsCamera.attachControl();
    fpsCamera.checkCollisions = true;
    fpsCamera.applyGravity = true;
    fpsCamera.minZ = 0.45; // avoid clipping collided objs
    fpsCamera.speed = 0.75;
    fpsCamera.angularSensibility = 4000;
    // fpsCamera.ellipsoid = new BABYLON.Vector3(0.5, 1, 0.5);

    // MAP
    const { meshes, particleSystems, skeletons, animationGroups } =
        await BABYLON.SceneLoader.ImportMeshAsync(
            "",
            "./",
            "Prototype_Level.glb",
            scene
        );
    meshes.forEach((mesh) => (mesh.checkCollisions = true));

    // MOUSE POINTER LOCK
    scene.onPointerDown = (evt) => {
        // left click
        if (evt.button === 0) {
            engine.enterPointerlock();
        }

        if (evt.button === 1) {
            engine.exitPointerlock();
        }
    };

    // WASD
    // keycode.info website
    // W
    fpsCamera.keysUp.push(87);
    // A
    fpsCamera.keysLeft.push(65);
    // S
    fpsCamera.keysDown.push(83);
    // D
    fpsCamera.keysRight.push(68);

    engine.hideLoadingUI();
    return scene;
};

export default createScene;
