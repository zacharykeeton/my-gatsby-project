import App from "./App";

const canvas = document.querySelector("#babylonCanvas") as HTMLCanvasElement;
const app = new App(canvas);
app.run();
