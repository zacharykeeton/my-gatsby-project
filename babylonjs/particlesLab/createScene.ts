import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";

function createScene(engine: BABYLON.Engine) {
    const scene = new BABYLON.Scene(engine);

    const camera = new BABYLON.ArcRotateCamera(
        "ArcRotateCamera",
        -Math.PI / 2,
        Math.PI / 2.2,
        10,
        new BABYLON.Vector3(0, 0, 0),
        scene
    );
    camera.attachControl(true);
    const light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );

    // Ground for positional reference
    const ground = BABYLON.MeshBuilder.CreateGround("ground", {
        width: 6,
        height: 6,
    });
    ground.material = new GridMaterial("groundMat", scene);
    ground.material.backFaceCulling = false;

    // Create a particle system
    const particleSystem = new BABYLON.ParticleSystem("particles", 500, scene);

    //Texture of each particle
    // Black background becomes transparent
    particleSystem.particleTexture = new BABYLON.Texture(
        "https://st.depositphotos.com/1400069/2216/i/450/depositphotos_22162671-stock-photo-snowflake-isolated-on-a-black.jpg",
        scene
    );

    // Position where the particles are emiited from
    // particleSystem.emitter = new BABYLON.Vector3(0, 0.5, 0);

    // set spawnable area
    particleSystem.minEmitBox = new BABYLON.Vector3(-3, 3, -3); // minimum box dimensions
    particleSystem.maxEmitBox = new BABYLON.Vector3(3, 3, 3); // maximum box dimensions

    // Snowflake size
    particleSystem.minSize = 0.125;
    particleSystem.maxSize = 0.25;

    // Snowflake lifespan
    particleSystem.minLifeTime = 2;
    particleSystem.maxLifeTime = 5;

    // Snowflake speed
    particleSystem.minEmitPower = -0.5;
    particleSystem.maxEmitPower = -1;

    // Max snowflakes emitted per frame
    particleSystem.emitRate = 50;

    // add noise
    var noiseTexture = new BABYLON.NoiseProceduralTexture("perlin", 256, scene);
    noiseTexture.animationSpeedFactor = 5;
    noiseTexture.persistence = 2;
    noiseTexture.brightness = 0.5;
    noiseTexture.octaves = 2;

    particleSystem.noiseTexture = noiseTexture;
    particleSystem.noiseStrength = new BABYLON.Vector3(1, 0.03, 1);

    particleSystem.start();

    return scene;
}

export default createScene;
