import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";

function createScene(engine: BABYLON.Engine) {
    const scene = new BABYLON.Scene(engine);

    const camera = new BABYLON.ArcRotateCamera(
        "ArcRotateCamera",
        -Math.PI / 2,
        Math.PI / 2.2,
        10,
        new BABYLON.Vector3(0, 0, 0),
        scene
    );
    camera.attachControl(true);
    const light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );

    // Ground for positional reference
    const ground = BABYLON.MeshBuilder.CreateGround("ground", {
        width: 6,
        height: 6,
    });
    ground.material = new GridMaterial("groundMat", scene);
    ground.material.backFaceCulling = false;

    /************ LAB CODE HERE /************/

    const box = BABYLON.MeshBuilder.CreateBox("box", {});
    box.position.x = 2;
    box.position.y = 0.5;

    const frameRate = 10;

    // create an Animation object targeting the
    // 'position.x' property
    const xSlide = new BABYLON.Animation(
        "xSlide", // animation name
        "position.x", // target property
        frameRate, // frames per second
        BABYLON.Animation.ANIMATIONTYPE_FLOAT, // Data Type of the changing value
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE // Loop mode
    );

    xSlide.setKeys([
        {
            frame: 0,
            value: 2,
        },
        {
            frame: frameRate,
            value: -2,
        },
        {
            frame: 2 * frameRate,
            value: 2,
        },
    ]);

    box.animations.push(xSlide);

    const ySlide = new BABYLON.Animation(
        "ySlide", // animation name
        "position.y", // target property
        frameRate, // frames per second
        BABYLON.Animation.ANIMATIONTYPE_FLOAT, // Data Type of the changing value
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE // Loop mode
    );

    ySlide.setKeys([
        {
            frame: 0,
            value: 1,
        },
        {
            frame: frameRate,
            value: -1,
        },
        {
            frame: 2 * frameRate,
            value: 1,
        },
    ]);
    box.animations.push(ySlide);

    const rotationXSlide = new BABYLON.Animation(
        "rotationXSlide", // animation name
        "rotation.x", // target property
        frameRate, // frames per second
        BABYLON.Animation.ANIMATIONTYPE_FLOAT, // Data Type of the changing value
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE // Loop mode
    );

    rotationXSlide.setKeys([
        {
            frame: 0,
            value: 0,
        },
        {
            frame: frameRate,
            value: -Math.PI / 2,
        },
        {
            frame: 2 * frameRate,
            value: 0,
        },
    ]);
    box.animations.push(rotationXSlide);

    scene.beginAnimation(box, 0, 2 * frameRate, true);
    // box.beginAnimation("xSlide", true);
    // box.beginAnimation()

    /************^^^ LAB CODE HERE ^^^ ************/

    return scene;
}

export default createScene;
