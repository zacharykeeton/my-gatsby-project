import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";

function createScene(engine: BABYLON.Engine) {
    const scene = new BABYLON.Scene(engine);

    const camera = new BABYLON.ArcRotateCamera(
        "ArcRotateCamera",
        -Math.PI / 2,
        Math.PI / 2.2,
        10,
        new BABYLON.Vector3(0, 0, 0),
        scene
    );
    camera.attachControl(true);
    const light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );

    // Ground for positional reference
    const ground = BABYLON.MeshBuilder.CreateGround("ground", {
        width: 6,
        height: 6,
    });
    ground.material = new GridMaterial("groundMat", scene);
    ground.material.backFaceCulling = false;

    return scene;
}

export default createScene;
