import * as BABYLON from "@babylonjs/core";
import createScene from "./createScene";
class App {
    private readonly _engine: BABYLON.Engine;
    private _activeScene: BABYLON.Scene | undefined;

    constructor(private readonly _canvas: HTMLCanvasElement) {
        this._engine = new BABYLON.Engine(this._canvas);
        window.addEventListener("resize", () => {
            this._engine.resize();
        });
    }
    async run() {
        this._activeScene = await createScene(this._engine);
        this._engine.runRenderLoop(() => {
            this._activeScene?.render();
        });
    }
}

export default App;
