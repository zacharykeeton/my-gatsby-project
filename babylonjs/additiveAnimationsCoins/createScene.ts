import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";
import { PhysicsEngine, HavokPlugin } from "@babylonjs/core/Physics";
import HavokPhysics from "@babylonjs/havok";

import makeTextPlane from "../shapesDemo/makeTextPlane";

function createGround(scene: BABYLON.Scene) {
  // GROUND
  // Ground for positional reference
  const ground = BABYLON.MeshBuilder.CreateGround("ground", {
    width: 60,
    height: 60,
  });
  ground.material = new GridMaterial("groundMat", scene);
  ground.material.backFaceCulling = false;
}

function createCamera(scene: BABYLON.Scene) {
  const camera = new BABYLON.ArcRotateCamera(
    "camera1",
    2.25,
    1,
    16,
    new BABYLON.Vector3(0, 1, 0),
    scene
  );
  camera.attachControl(true);
  camera.lowerRadiusLimit = 2;
  // camera.upperRadiusLimit = 10;
  camera.wheelDeltaPercentage = 0.01;
}

function createHemisphericLight(scene: BABYLON.Scene) {
  const light = new BABYLON.HemisphericLight(
    "light1",
    new BABYLON.Vector3(0, 1, 0),
    scene
  );
  light.intensity = 0.6;
  light.specular = BABYLON.Color3.Black();
}

function createDirectionalLight(scene: BABYLON.Scene) {
  const light2 = new BABYLON.DirectionalLight(
    "dir01",
    new BABYLON.Vector3(0, -0.5, -1.0),
    scene
  );
  light2.position = new BABYLON.Vector3(0, 5, 5);
  return light2;
}

async function createScene(engine: BABYLON.Engine) {
  // engine.enableOfflineSupport = false;
  // engine.displayLoadingUI();
  var scene = new BABYLON.Scene(engine);

  // initialize plugin
  const havokInterface = await HavokPhysics();
  const hk = new HavokPlugin(undefined, havokInterface);
  // enable physics in the scene with a gravity
  scene.enablePhysics(new BABYLON.Vector3(0, -9.8, 0), hk);

  // Camera
  createCamera(scene);

  // Lights
  createHemisphericLight(scene);
  const light2 = createDirectionalLight(scene);

  // TODO: learn about skybox, ground
  const createDefaultEnvironmentHelper = scene.createDefaultEnvironment({
    enableGroundShadow: true,
  })!;
  createDefaultEnvironmentHelper.setMainColor(BABYLON.Color3.Gray());
  if (createDefaultEnvironmentHelper.ground?.position) {
    createDefaultEnvironmentHelper.ground.position.y += 0.01;
  }
  const groundAggregate = new BABYLON.PhysicsAggregate(
    createDefaultEnvironmentHelper.ground!,
    BABYLON.PhysicsShapeType.BOX,
    { mass: 0 },
    scene
  );

  // IMPORT XBOT MODEL
  // Model by Mixamo
  const { meshes, skeletons, particleSystems } =
    await BABYLON.SceneLoader.ImportMeshAsync(
      "",
      "./",
      "Xbot.glb", // note: needed to import loader
      scene
    );

  // console.log(meshes);
  const xbot = meshes[0];
  // xbot.getChildMeshes().forEach((childMesh) => (childMesh.visibility = 1));
  // xbot.checkCollisions = true;
  const dummyPhysicsRoot = BABYLON.MeshBuilder.CreateCapsule(
    "dummyPhysicsRoot",
    { height: 2 },
    scene
  );
  // var dummyPhysicsRoot = new BABYLON.MeshBuilder.create("dummyPhysicsRoot", {size: 1, height: 2, width: 1}, scene);
  dummyPhysicsRoot.addChild(xbot);
  // DummyPhysicsRoot Visibility Change to 0 to Hide
  dummyPhysicsRoot.visibility = 0.2;
  dummyPhysicsRoot.position.y = 1;
  xbot.position.y = -1;

  const dummyAggregate = new BABYLON.PhysicsAggregate(
    dummyPhysicsRoot,
    BABYLON.PhysicsShapeType.BOX,
    { mass: 1000, restitution: 0.01 },
    scene
  );
  dummyAggregate.body.setMotionType(BABYLON.PhysicsMotionType.DYNAMIC);
  dummyAggregate.body.setMassProperties({
    inertia: new BABYLON.Vector3(0, 0, 0),
  });

  const promptBox = BABYLON.MeshBuilder.CreateBox("promptBox", {}, scene);
  promptBox.position = new BABYLON.Vector3(-1, 0.5, -1);
  // promptBox.checkCollisions = true;
  promptBox.material = new BABYLON.StandardMaterial("promptBoxMat", scene);
  (promptBox.material as BABYLON.StandardMaterial).diffuseColor =
    BABYLON.Color3.Blue();
  const promptBoxAggregate = new BABYLON.PhysicsAggregate(
    promptBox,
    BABYLON.PhysicsShapeType.MESH,
    { mass: 0, restitution: 0 },
    scene
  );

  const box = BABYLON.MeshBuilder.CreateBox("box", {}, scene);
  box.position = new BABYLON.Vector3(1, 0.5, 1);
  // box.checkCollisions = true;
  const boxAggregate = new BABYLON.PhysicsAggregate(
    box,
    BABYLON.PhysicsShapeType.MESH,
    { mass: 1, restitution: 0.8 },
    scene
  );

  const box2 = BABYLON.MeshBuilder.CreateBox("box1", {}, scene);
  box2.position = new BABYLON.Vector3(2.5, 0.5, 2.5);
  const box2Aggregate = new BABYLON.PhysicsAggregate(
    box2,
    BABYLON.PhysicsShapeType.MESH,
    { mass: 1, restitution: 0.8 },
    scene
  );

  const box3 = BABYLON.MeshBuilder.CreateBox("box2", {}, scene);
  box3.position = new BABYLON.Vector3(4, 5.5, 4);
  const box3Aggregate = new BABYLON.PhysicsAggregate(
    box3,
    BABYLON.PhysicsShapeType.MESH,
    { mass: 1, restitution: 0.8 },
    scene
  );

  // Shadows
  const shadowGenerator = new BABYLON.ShadowGenerator(1024, light2);
  shadowGenerator.useBlurExponentialShadowMap = true;
  shadowGenerator.blurKernel = 32;
  shadowGenerator.addShadowCaster(meshes[0], true);
  shadowGenerator.addShadowCaster(box, true);
  shadowGenerator.addShadowCaster(box2, true);
  shadowGenerator.addShadowCaster(box3, true);
  for (var index = 0; index < meshes.length; index++) {
    meshes[index].receiveShadows = false;
  }

  // Initialize override animations, turn on idle by default
  const idleAnim = scene.animationGroups.find((a) => a.name === "idle")!;
  const idleParam = { name: "Idle", anim: idleAnim, weight: 1 };
  idleAnim.weight = 1;
  idleAnim.play(true);

  const walkAnim = scene.animationGroups.find((a) => a.name === "walk")!;
  const walkParam = { name: "Walk", anim: walkAnim, weight: 0 };
  walkAnim.weight = 0;
  walkAnim.play(true);

  const runAnim = scene.animationGroups.find((a) => a.name === "run")!;
  const runParam = { name: "Run", anim: runAnim, weight: 0 };
  runAnim.weight = 0;
  runAnim.play(true);

  // Initialize additive poses. Slice of reference pose at first frame

  // SAD POSE
  const sadPoseAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
    scene.animationGroups.find((a) => a.name === "sad_pose")!
  );
  const sadPoseParam = {
    name: "Sad Pose",
    anim: sadPoseAnim,
    weight: 0,
  };
  sadPoseAnim.weight = 0;
  sadPoseAnim.start(
    true,
    1,
    2.0000001043081284, //0.03333333507180214 * 60,
    2.0000001043081284 //0.03333333507180214 * 60
  );

  // SNEAK POSE
  const sneakPoseAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
    scene.animationGroups.find((a) => a.name === "sneak_pose")!
  );
  const sneakPoseParam = {
    name: "Sneak Pose",
    anim: sneakPoseAnim,
    weight: 0,
  };
  sneakPoseAnim.weight = 0;
  sneakPoseAnim.start(
    true,
    1,
    0.03333333507180214 * 60,
    0.03333333507180214 * 60
  );

  // Initialize additive animations
  const headShakeAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
    scene.animationGroups.find((a) => a.name === "headShake")!
  );
  const headShakeParam = {
    name: "Head Shake",
    anim: headShakeAnim,
    weight: 0,
  };
  headShakeAnim.weight = 0;
  headShakeAnim.play(true);

  const agreeAnim = BABYLON.AnimationGroup.MakeAnimationAdditive(
    scene.animationGroups.find((a) => a.name === "agree")!
  );
  const agreeParam = { name: "Agree", anim: agreeAnim, weight: 0 };
  agreeAnim.weight = 0;
  agreeAnim.play(true);

  // UI
  var advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
  var UiPanel = new GUI.StackPanel();
  UiPanel.isVisible = false;
  UiPanel.width = "220px";
  UiPanel.fontSize = "14px";
  UiPanel.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
  UiPanel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_CENTER;
  advancedTexture.addControl(UiPanel);
  UiPanel.background = "blue";

  // Keep track of the current override animation
  let currentParam:
    | { name: string; anim: BABYLON.AnimationGroup; weight: number }
    | undefined = idleParam;

  function onBeforeAnimation() {
    // Increment the weight of the current override animation
    if (currentParam) {
      currentParam.weight = BABYLON.Scalar.Clamp(
        currentParam.weight + 0.01,
        0,
        1
      );
      currentParam.anim.weight = currentParam.weight;
    }

    // Decrement the weight of all override animations that aren't current
    if (currentParam !== idleParam) {
      idleParam.weight = BABYLON.Scalar.Clamp(idleParam.weight - 0.01, 0, 1);
      idleParam.anim.weight = idleParam.weight;
    }

    if (currentParam !== walkParam) {
      walkParam.weight = BABYLON.Scalar.Clamp(walkParam.weight - 0.01, 0, 1);
      walkParam.anim.weight = walkParam.weight;
    }

    if (currentParam !== runParam) {
      runParam.weight = BABYLON.Scalar.Clamp(runParam.weight - 0.01, 0, 1);
      runParam.anim.weight = runParam.weight;
    }

    // Remove the callback the current animation weight reaches 1 or
    // when all override animations reach 0 when current is undefined
    if (
      (currentParam && currentParam.weight === 1) ||
      (idleParam.weight === 0 &&
        walkParam.weight === 0 &&
        runParam.weight === 0)
    ) {
      scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    }
  }

  // Button for blending to bind pose
  var button = GUI.Button.CreateSimpleButton("but0", "None");
  button.paddingTop = "10px";
  button.width = "100px";
  button.height = "50px";
  button.color = "white";
  button.background = "green";
  button.onPointerDownObservable.add(function () {
    // Remove current animation
    currentParam = undefined;

    // Restart animation observer
    scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
  });
  UiPanel.addControl(button);

  // Button for blending to idle
  var button = GUI.Button.CreateSimpleButton("but0", "Idle");
  button.paddingTop = "10px";
  button.width = "100px";
  button.height = "50px";
  button.color = "white";
  button.background = "green";
  button.onPointerDownObservable.add(function () {
    // Do nothing if idle is already the current animation
    if (currentParam === idleParam) {
      return;
    }

    // Restart animation observer with idle set to current
    scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    currentParam = idleParam;
    scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
  });
  UiPanel.addControl(button);

  // Button for blending to walk
  button = GUI.Button.CreateSimpleButton("but0", "Walk");
  button.paddingTop = "10px";
  button.width = "100px";
  button.height = "50px";
  button.color = "white";
  button.background = "green";
  button.onPointerDownObservable.add(function () {
    // Do nothing if walk is already the current animation
    if (currentParam === walkParam) {
      return;
    }

    // Synchronize animations
    if (currentParam) {
      walkParam.anim.syncAllAnimationsWith(null);
      currentParam.anim.syncAllAnimationsWith(walkParam.anim.animatables[0]);
    }

    // Restart animation observer with walk set to current
    scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    currentParam = walkParam;
    scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
  });
  UiPanel.addControl(button);

  // Button for blending to run
  button = GUI.Button.CreateSimpleButton("but0", "Run");
  button.paddingTop = "10px";
  button.width = "100px";
  button.height = "50px";
  button.color = "white";
  button.background = "green";
  button.onPointerDownObservable.add(function () {
    // Do nothing if run is already the current animation
    if (currentParam === runParam) {
      return;
    }

    // Synchronize animations
    if (currentParam) {
      runParam.anim.syncAllAnimationsWith(null);
      currentParam.anim.syncAllAnimationsWith(runParam.anim.animatables[0]);
    }

    // Restart animation observer with run set to current
    scene.onBeforeAnimationsObservable.removeCallback(onBeforeAnimation);
    currentParam = runParam;
    scene.onBeforeAnimationsObservable.add(onBeforeAnimation);
  });
  UiPanel.addControl(button);

  // Create a slider to control the weight of each additive pose/animation
  var params = [sadPoseParam, sneakPoseParam, headShakeParam, agreeParam];
  params.forEach((param) => {
    // Label
    var header = new GUI.TextBlock();
    header.text = param.name + ":" + param.weight.toFixed(2);
    header.height = "40px";
    header.color = "green";
    header.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    header.paddingTop = "10px";
    UiPanel.addControl(header);

    // Slider
    var slider = new GUI.Slider();
    slider.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    slider.minimum = 0;
    slider.maximum = 1;
    slider.color = "green";
    slider.value = param.anim.weight;
    slider.height = "20px";
    slider.width = "205px";
    UiPanel.addControl(slider);

    // Update animation weight value according to slider value
    slider.onValueChangedObservable.add((v) => {
      param.anim.weight = v;
      param.weight = v;
      header.text = param.name + ":" + param.weight.toFixed(2);
    });
    // param.anim._slider = slider;
    slider.value = param.weight;
  });

  // CAPTURE KEYPRESSES
  // Keyboard events
  let keydowns = 0;
  const inputMap: any = {};
  scene.actionManager = new BABYLON.ActionManager(scene);
  scene.actionManager.registerAction(
    new BABYLON.ExecuteCodeAction(
      BABYLON.ActionManager.OnKeyDownTrigger,
      function (evt) {
        console.log(evt.sourceEvent.type, ++keydowns);
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";

        console.log(inputMap);
      }
    )
  );
  scene.actionManager.registerAction(
    new BABYLON.ExecuteCodeAction(
      BABYLON.ActionManager.OnKeyUpTrigger,
      function (evt) {
        console.log(
          evt.sourceEvent.key,
          evt.sourceEvent.key.toUpperCase(),
          evt.sourceEvent.key.toLowerCase()
        );

        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";

        inputMap[evt.sourceEvent.key.toUpperCase()] =
          evt.sourceEvent.type == "keydown";

        inputMap[evt.sourceEvent.key.toLowerCase()] =
          evt.sourceEvent.type == "keydown";
      }
    )
  );

  // MOVEMENT
  let hero = dummyAggregate.body;
  let player = dummyAggregate;
  let characterIsIdle = true;
  const heroWalkSpeed = 0.028;
  const heroRunSpeed = heroWalkSpeed * 2;
  let heroSpeed = heroWalkSpeed;
  const heroSpeedBackwards = 0.01;
  const heroRotationSpeed = 0.1;
  //Rendering loop (executed for everyframe)
  const playerSpeed = 0.1;

  // Define player movement speed
  let moveForward = 0;
  let moveDirection = 0;
  let forceMagnitude = 4;
  let rotationSpeed = 2;
  let angularDamping = 50;
  let decelerationFactor = 0.2;
  let moveX, moveZ;
  let animationState = 0;
  let movingForward = false;
  let movingBackward = false;

  scene.onBeforeRenderObservable.add(() => {
    // if (xbot.intersectsMesh(box)) {
    //     console.log("TOUCHING");
    // }
    let keydown = false; // helps with animations
    // MOVEMENTS

    // WALKING
    // WALKING FORWARD
    if (inputMap["w"]) {
      console.log("walk forward");
      // hero.moveWithCollisions(hero.forward.scaleInPlace(heroWalkSpeed));
      keydown = true;
      movingForward = true;

      var frontVector = player.transformNode.getDirection(BABYLON.Axis.Z);
      player.body.setLinearVelocity(frontVector.scale(forceMagnitude));

      // const forward = BABYLON.Vector3.TransformCoordinates(
      //     new BABYLON.Vector3(0, 0, playerSpeed), // delta-z forward
      //     BABYLON.Matrix.RotationY(hero.transformNode.rotation.y)
      // );
      // hero.transformNode.position.addInPlace(forward);
    }

    // WALKING BACKWARD
    if (inputMap["s"]) {
      console.log("walk backward");

      // hero.moveWithCollisions(
      //     hero.forward.scaleInPlace(-heroSpeedBackwards)
      // );
      var frontVector = player.transformNode.getDirection(BABYLON.Axis.Z);
      player.body.setLinearVelocity(frontVector.scale(-forceMagnitude));
      keydown = true;
      movingBackward = true;
    }

    // WALKING TURN LEFT
    if (inputMap["a"]) {
      // hero.rotate(BABYLON.Vector3.Up(), -heroRotationSpeed);
      keydown = true;
      moveDirection = 1;
      // console.log("TURN LEFT");

      const rotationAxis = new BABYLON.Vector3(0, -1, 0); // el eje de rotación
      player.body.setAngularDamping(angularDamping);
      player.body.setAngularVelocity(rotationAxis.scale(rotationSpeed));
    }

    // WALKING TURN RIGHT
    if (inputMap["d"]) {
      // console.log("TURN RIGHTT");
      // hero.rotate(BABYLON.Vector3.Up(), heroRotationSpeed);
      keydown = true;
      moveDirection = -1;

      const rotationAxis = new BABYLON.Vector3(0, 1, 0); // el eje de rotación
      player.body.setAngularDamping(angularDamping);
      player.body.setAngularVelocity(rotationAxis.scale(rotationSpeed));
    }

    // // RUNNING
    // // // RUNNING wwwwwwwwwwwwwWWWWWWWWWWWwwFORWARD
    // if (inputMap["Shift"] && (inputMap["W"] || inputMap["w"])) {
    //     hero.moveWithCollisions(hero.forward.scaleInPlace(heroRunSpeed));
    //     keydown = true;
    // }

    // // RUNNING BACKWARD
    // if (inputMap["Shift"] && inputMap["S"]) {
    //     hero.moveWithCollisions(
    //         hero.forward.scaleInPlace(-heroSpeedBackwards)
    //     );
    //     keydown = true;
    // }

    // // RUNNING TURN LEFT
    // if (inputMap["Shift"] && inputMap["A"]) {
    //     hero.rotate(BABYLON.Vector3.Up(), -heroRotationSpeed);
    //     keydown = true;
    //     // console.log("TURN LEFT");
    // }

    // // RUNNING TURN RIGHT
    // if (inputMap["Shift"] && inputMap["D"]) {
    //     // console.log("TURN RIGHTT");
    //     hero.rotate(BABYLON.Vector3.Up(), heroRotationSpeed);
    //     keydown = true;
    // }

    // Stopping
    if (!keydown) {
      console.log("NO KEY DOWN");
      const frontVector = player.transformNode.getDirection(BABYLON.Axis.Z);
      if (movingForward) {
        console.log("STOP FORWARD");
        // start slowing down
        player.body.setLinearVelocity(frontVector.scale(decelerationFactor));

        // player.body.

        movingForward = false;
      }

      if (movingBackward) {
        player.body.setLinearVelocity(frontVector.scale(-decelerationFactor));

        movingBackward = false;
      }

      if (moveDirection == 0) {
        const rotationAxis = new BABYLON.Vector3(0, 0, 0); // el eje de rotación
        player.body.setAngularVelocity(rotationAxis.scale(0));
      }
      moveDirection = 0;
    }

    // ANIMATIONS

    if (keydown) {
      // if keydown and character was idle
      // start an animation
      if (characterIsIdle) {
        // Stop being idle
        idleAnim.weight = 0;
        characterIsIdle = false;

        // Start walking animation
        // walkAnim.weight = 1;
        walkAnim.weight = 1;
      } else {
        if (inputMap["Shift"] && (inputMap["W"] || inputMap["w"])) {
          walkAnim.weight = 0;
          runAnim.weight = 1;
          console.log("RUNNNNN");
        } else {
          runAnim.weight = 0;
          walkAnim.weight = 1;
        }
      }
    } else {
      // if key is not pressed and
      // character is not idle
      // become idle
      if (!characterIsIdle) {
        // stop other animations
        walkAnim.weight = 0;
        runAnim.weight = 0;

        // become idle
        idleAnim.weight = 1;
        characterIsIdle = true;
      }
    }
  });

  engine.hideLoadingUI();
  return scene;
}

export default createScene;
