// TEXTBOOK
// https://research.ncl.ac.uk/game/mastersdegree/graphicsforgames/

import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";
import { PhysicsEngine, HavokPlugin } from "@babylonjs/core/Physics";
import HavokPhysics from "@babylonjs/havok";

import makeTextPlane from "../shapesDemo/makeTextPlane";

var mapSize = 80;
var mouseSensitivity = 0.0002;

function mouseMove(
  evt: MouseEvent,
  engine: BABYLON.Engine,
  playerMesh: BABYLON.AbstractMesh
) {
  const deltaTime = engine.getDeltaTime();
  playerMesh.rotation.x += evt.movementY * deltaTime * mouseSensitivity;
  playerMesh.rotation.y -= evt.movementX * deltaTime * mouseSensitivity;
}

function setupPointerLock(
  engine: BABYLON.Engine,
  playerMesh: BABYLON.AbstractMesh
) {
  // when element is clicked, we're going to request a
  // pointerlock
  const canvas = document.querySelector("#babylonCanvas") as HTMLCanvasElement;
  canvas.onclick = function () {
    // Ask the browser to lock the pointer)
    canvas.requestPointerLock();
  };

  const mouseMoveCallback = (evt: MouseEvent) => {
    console.log("MOUSE MOVE");
    mouseMove(evt, engine, playerMesh);
  };

  // register the callback when a pointerlock event occurs
  let pointerAlreadyLocked = false;
  document.addEventListener(
    "pointerlockchange",
    (evt) => {
      console.log("pointerlockchange");

      if (document.pointerLockElement === canvas) {
        if (pointerAlreadyLocked) {
          console.log("POINTER ALREADY LOCKED");
          return;
        }
        document.addEventListener("mousemove", mouseMoveCallback, false);
        pointerAlreadyLocked = true;
        console.log("POINTER NOW LOCKED");
      } else {
        console.log("POINTER UNLOCKED");
        document.removeEventListener("mousemove", mouseMoveCallback, false);
        pointerAlreadyLocked = false;
      }
    },
    false
  );
}

function setupScene(engine: BABYLON.Engine) {
  // This creates a basic Babylon Scene object (non-mesh)
  var scene = new BABYLON.Scene(engine);
  scene.useRightHandedSystem = true;

  const assumedFramesPerSecond = 60;
  const earthGravity = -9.81;
  scene.gravity = new BABYLON.Vector3(
    0,
    earthGravity / assumedFramesPerSecond,
    0
  );

  ////// COLLISIONS STEP 3 of 3: Apply collision
  // Enable collisions in scene
  scene.collisionsEnabled = true;

  return scene;
}

function createCamera(scene: BABYLON.Scene) {
  // First person camera
  const fpsCamera = new BABYLON.UniversalCamera(
    "FPS",
    new BABYLON.Vector3(0, 0, 0),
    scene
  );
  fpsCamera.minZ = 0.0001;
  return fpsCamera;
}

function createGround(scene: BABYLON.Scene) {
  // Ground material
  const groundMaterial = new BABYLON.StandardMaterial("groundMaterial", scene);
  groundMaterial.diffuseTexture = new BABYLON.Texture(
    "./textures/HADnUQr.png",
    scene
  ) as BABYLON.Texture;
  (groundMaterial.diffuseTexture as BABYLON.Texture).uScale = mapSize;
  (groundMaterial.diffuseTexture as BABYLON.Texture).vScale = mapSize;
  groundMaterial.specularColor = BABYLON.Color3.Black();
  groundMaterial.emissiveColor = BABYLON.Color3.White();
  // Ground
  const ground = BABYLON.MeshBuilder.CreateGround(
    "ground",
    { height: mapSize, width: mapSize, subdivisions: 4 },
    scene
  );
  ground.material = groundMaterial;
  ground.checkCollisions = true;
}

function createSkybox(scene: BABYLON.Scene) {
  // Skybox material

  var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);

  // to show the images on the inside of the box
  skyboxMaterial.backFaceCulling = false;

  // to pull the _px _nx, etc images (.jpg)
  // CubeTexture can only be applied to Reflection texture
  skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(
    "./textures/skybox",
    scene
  );

  // To place the correct images on the correct sides
  skyboxMaterial.reflectionTexture.coordinatesMode =
    BABYLON.Texture.SKYBOX_MODE;

  // set diffuse and specular color to black else it'll
  // be too bright
  skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
  skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);

  // Create the big box. The scene will be
  // inside the box
  var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, scene);
  skybox.material = skyboxMaterial;
}

function createLight(scene: BABYLON.Scene) {
  const light = new BABYLON.HemisphericLight(
    "light",
    new BABYLON.Vector3(0, 1, 0),
    scene
  );
  // Default intensity is 1. Let's dim the light a small amount
  light.intensity = 0.7;
}

function createBoxes(scene: BABYLON.Scene) {
  function getRandomIntInclusive(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /*  Box material
        2DO: material (instance) https://forum.babylonjs.com/t/shader-material-doesnt-work-with-instances/793/14 */
  const mapBoxMaterial = new BABYLON.StandardMaterial("mapBoxMaterial", scene);
  mapBoxMaterial.diffuseTexture = new BABYLON.Texture(
    "./textures/HADnUQr.png",
    scene
  );
  mapBoxMaterial.specularColor = BABYLON.Color3.Black();
  mapBoxMaterial.emissiveColor = BABYLON.Color3.Green();

  //  Generate box root
  const box = BABYLON.MeshBuilder.CreateBox("box", { size: 1 }, scene);
  box.isVisible = false;
  box.material = mapBoxMaterial;

  // Generate box instance
  // Creating identical meshes via instancing is more
  // performant. You can then set position, rotation,
  // scaling for each.
  for (let index = 0; index < mapSize / 2; index++) {
    const newInstance = box.createInstance("box" + index);
    newInstance.position = new BABYLON.Vector3(
      getRandomIntInclusive(-mapSize / 2, mapSize / 2),
      0.5,
      getRandomIntInclusive(-mapSize / 2, mapSize / 2)
    );
    newInstance.checkCollisions = true;
  }
}
async function createPlayer(scene: BABYLON.Scene) {
  // Load arms/gun mesh
  const { meshes, particleSystems, skeletons, animationGroups } =
    await BABYLON.SceneLoader.ImportMeshAsync(
      "",
      "https://dl.dropbox.com/s/wbia4zetn1bdeze/",
      "arms_assault_rifle_02.glb",
      scene
    );
  // Hide unused attachments
  scene.getMeshByName("knife")?.setEnabled(false);
  scene.getMeshByName("assault_rifle_02_iron_sights")?.setEnabled(false);
  scene.getMeshByName("bullet")?.setEnabled(false);
  scene.getMeshByName("scope_01")?.setEnabled(false);
  scene.getMeshByName("scope_02")?.setEnabled(false);
  scene.getNodeByName("scope_03")?.setEnabled(false);
  scene.getNodeByName("silencer")?.setEnabled(false);

  const cameraNode = scene.getNodeByName("camera")!;
  const fpsMesh = meshes[0];
  fpsMesh.position.y = 1;
  fpsMesh.rotation = BABYLON.Vector3.Zero();

  //Get animations
  const aim_fire_pose = scene.getAnimationGroupByName("aim_fire_pose");
  const idle = scene.getAnimationGroupByName("idle")!;
  const walk = scene.getAnimationGroupByName("walk")!;
  //Play idle animation
  idle.start(true, 1.0, idle.from, idle.to, false);

  return { playerMesh: fpsMesh, cameraNode };
}
function createThing(scene: BABYLON.Scene) {}

const createScene = async function (engine: BABYLON.Engine) {
  engine.enableOfflineSupport = false;
  engine.displayLoadingUI();

  const scene = setupScene(engine);
  const fpsCamera = createCamera(scene);
  createLight(scene);
  createGround(scene);
  createSkybox(scene);
  createBoxes(scene);

  // // PLAYER
  const { playerMesh, cameraNode } = await createPlayer(scene);

  // connect camera to player
  fpsCamera.parent = cameraNode;

  // move player
  // CAPTURE KEYPRESSES
  // Keyboard events
  const inputMap: any = {};
  scene.actionManager = new BABYLON.ActionManager(scene);
  // KEYDOWNS
  scene.actionManager.registerAction(
    new BABYLON.ExecuteCodeAction(
      BABYLON.ActionManager.OnKeyDownTrigger,
      function (evt) {
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";

        console.log(inputMap);
      }
    )
  );
  //KEYUPS
  scene.actionManager.registerAction(
    new BABYLON.ExecuteCodeAction(
      BABYLON.ActionManager.OnKeyUpTrigger,
      function (evt) {
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
        console.log(inputMap);
      }
    )
  );

  // BODY MOVEMENT
  let hero = playerMesh;
  const heroWalkSpeed = 0.2;
  //Rendering loop (executed for everyframe)
  scene.onBeforeRenderObservable.add(() => {
    let keydown = false; // helps with animations

    const playerSpeed = 0.1;

    // MOVEMENTS

    // WALKING
    // WALKING FORWARD
    if (inputMap["w"]) {
      // hero.moveWithCollisions(hero.forward.scaleInPlace(-heroWalkSpeed));
      keydown = true;

      // determine a direction vector in terms
      // of a give rotation
      // i.e. what direction is 'forward' given
      // the players current .y orientation
      const forward = BABYLON.Vector3.TransformCoordinates(
        new BABYLON.Vector3(0, 0, playerSpeed), // delta-z forward
        BABYLON.Matrix.RotationY(playerMesh.rotation.y)
      );
      playerMesh.position.addInPlace(forward);
    }

    // WALKING BACKWARD
    if (inputMap["s"]) {
      // hero.moveWithCollisions(hero.forward.scaleInPlace(heroWalkSpeed));
      keydown = true;

      const backward = BABYLON.Vector3.TransformCoordinates(
        new BABYLON.Vector3(0, 0, -playerSpeed), // delta-z backward
        BABYLON.Matrix.RotationY(playerMesh.rotation.y)
      );
      playerMesh.position.addInPlace(backward);
    }

    // WALKING STRAFE LEFT
    if (inputMap["a"]) {
      // hero.moveWithCollisions(hero.right.scaleInPlace(-heroWalkSpeed));
      keydown = true;

      const left = BABYLON.Vector3.TransformCoordinates(
        new BABYLON.Vector3(playerSpeed, 0, 0), // delta-z left
        BABYLON.Matrix.RotationY(playerMesh.rotation.y)
      );
      playerMesh.position.addInPlace(left);
    }

    // WALKING STRAFE RIGHT
    if (inputMap["d"]) {
      // hero.moveWithCollisions(hero.right.scaleInPlace(heroWalkSpeed));
      keydown = true;
      const right = BABYLON.Vector3.TransformCoordinates(
        new BABYLON.Vector3(-playerSpeed, 0, 0), // delta-z right
        BABYLON.Matrix.RotationY(playerMesh.rotation.y)
      );
      playerMesh.position.addInPlace(right);
    }

    //     if (keys.left) {
    //         var left = BABYLON.Vector3.TransformCoordinates(
    //             new BABYLON.Vector3(0.1, 0, 0),
    //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
    //         );
    //         fpsMesh.position.addInPlace(left);
    //     }
    //     if (keys.right) {
    //         var right = BABYLON.Vector3.TransformCoordinates(
    //             new BABYLON.Vector3(-0.1, 0, 0),
    //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
    //         );
    //         fpsMesh.position.addInPlace(right);
    //     }
    //     if (keys.forward) {
    //         var forward = BABYLON.Vector3.TransformCoordinates(
    //             new BABYLON.Vector3(0, 0, 0.1),
    //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
    //         );
    //         fpsMesh.position.addInPlace(forward);
    //     }
    //     if (keys.back) {
    //         var back = BABYLON.Vector3.TransformCoordinates(
    //             new BABYLON.Vector3(0, 0, -0.1),
    //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
    //         );
    //         fpsMesh.position.addInPlace(back);
    //     }

    // if(keydown){
    //     walk.start(true, 1.0, walk.from, walk.to, false);
    // }else{

    // }
  });

  // HEAD MOVEMENT
  setupPointerLock(engine, playerMesh);

  // FIRE BULLET
  // create root instance
  const bullet = BABYLON.MeshBuilder.CreateSphere(
    "bullet",
    { segments: 3, diameter: 0.3 },
    scene
  );
  bullet.material = new BABYLON.StandardMaterial("texture1", scene);
  (bullet.material as BABYLON.StandardMaterial).diffuseColor =
    new BABYLON.Color3(2, 0, 0);
  bullet.isVisible = false; // not the same as visibility = 0

  scene.onPointerObservable.add((pointerInfo) => {
    if (pointerInfo.type === BABYLON.PointerEventTypes.POINTERDOWN) {
      const newBullet = bullet.createInstance("newBullet");
      newBullet.position = fpsCamera.globalPosition.clone();

      // Class used to store matrix data (4x4)
      // Matrix size is given by rows x columns.
      // A Vector3 is a 1 X 3 matrix [x, y, z].
      /*
                [
                    0,0,0,0
                    0,0,0,0
                    0,0,0,0
                    0,0,0,0
                ]
            */
      // https://doc.babylonjs.com/typedoc/classes/BABYLON.Matrix
      // const invertedCameraViewMatrix = new BABYLON.Matrix();

      const cameraViewMatrix = fpsCamera.getViewMatrix();

      // What is the inverse of a matrix
      // When we multiply a matrix by its inverse we get the
      // Identity Matrix (which is like "1" for matrices):
      // I =
      /*
                [
                    1,0,0,0
                    0,1,0,0
                    0,0,1,0
                    0,0,0,1
                ]
            */
      // The inverse of A is A-1 only when:
      // AA-1 = A-1A = I
      // Sometimes there is no inverse at all.

      // See page 2 here:
      // https://research.ncl.ac.uk/game/mastersdegree/graphicsforgames/theviewmatrix/Tutorial%202%20-%20The%20View%20Matrix.pdf
      const invertedCameraViewMatrix = cameraViewMatrix.invert();

      // apply the invertedCameraView position and rotaton, etc
      // to a -z vector
      const direction = BABYLON.Vector3.TransformNormal(
        new BABYLON.Vector3(0, 0, -1),
        invertedCameraViewMatrix
      );

      // Normalize
      // Makes this vector have a magnitude of 1.
      // When normalized, a vector keeps the same direction but its length is 1.0.
      // A vector is an object that has both a magnitude and a
      // direction. Geometrically, we can picture a vector as a
      // directed line segment, whose length is the magnitude of the
      // vector and with an arrow indicating the direction. The
      // direction of the vector is from its tail to its head.
      // https://mathinsight.org/vector_introduction
      // When normalized, a vector keeps the same direction
      // but its length is 1.0
      direction.normalize();

      scene.registerBeforeRender(function () {
        // use direction.scale(0.1) to slow down
        // bullets or speed up with .scale(2),etc
        // scaling a vector just changes the magnitude
        newBullet.position.addInPlace(direction);
      });
    }
  });

  // walk.start(true, 1.0, walk.from, walk.to, false);
  // aim_fire_pose.start(true, 1.0, aim_fire_pose.from, aim_fire_pose.to, false);

  /* https://www.babylonjs-playground.com/#1RHSYU#1 */
  // fpsMesh.update = function () {
  //     //2DO: remove this by updating model in blender
  //     fpsMesh.position.y = 1;

  //     if (keys.left) {
  //         var left = BABYLON.Vector3.TransformCoordinates(
  //             new BABYLON.Vector3(0.1, 0, 0),
  //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
  //         );
  //         fpsMesh.position.addInPlace(left);
  //     }
  //     if (keys.right) {
  //         var right = BABYLON.Vector3.TransformCoordinates(
  //             new BABYLON.Vector3(-0.1, 0, 0),
  //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
  //         );
  //         fpsMesh.position.addInPlace(right);
  //     }
  //     if (keys.forward) {
  //         var forward = BABYLON.Vector3.TransformCoordinates(
  //             new BABYLON.Vector3(0, 0, 0.1),
  //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
  //         );
  //         fpsMesh.position.addInPlace(forward);
  //     }
  //     if (keys.back) {
  //         var back = BABYLON.Vector3.TransformCoordinates(
  //             new BABYLON.Vector3(0, 0, -0.1),
  //             BABYLON.Matrix.RotationY(fpsMesh.rotation.y)
  //         );
  //         fpsMesh.position.addInPlace(back);
  //     }

  //     // diffAngle = null;
  //     forward = null;
  // };

  //         // Event listener for WASD movement keys
  //         window.addEventListener("keydown", handleKeyDown, false);
  //         window.addEventListener("keyup", handleKeyUp, false);

  //         function handleKeyDown(evt) {
  //             if (evt.keyCode == 65) {
  //                 //A
  //                 keys.left = true;
  //                 console.log("A: " + keys.left);
  //                 walk.start(true, 1.0, walk.from, walk.to, false);
  //             }
  //             if (evt.keyCode == 68) {
  //                 //D
  //                 keys.right = true;
  //                 console.log("D: " + keys.right);
  //                 walk.start(true, 1.0, walk.from, walk.to, false);
  //             }
  //             if (evt.keyCode == 87) {
  //                 //W
  //                 keys.forward = true;
  //                 console.log("W: " + keys.forward);
  //                 walk.start(true, 1.0, walk.from, walk.to, false);
  //             }
  //             if (evt.keyCode == 83) {
  //                 //S
  //                 keys.back = true;
  //                 console.log("S: " + keys.back);
  //                 walk.start(true, 1.0, walk.from, walk.to, false);
  //             }
  //         }

  //         function handleKeyUp(evt) {
  //             if (evt.keyCode == 65) {
  //                 //A
  //                 keys.left = false;
  //                 if (!Object.values(keys).includes(true)) {
  //                     walk.stop();
  //                 }
  //             }
  //             if (evt.keyCode == 68) {
  //                 //D
  //                 keys.right = false;
  //                 if (!Object.values(keys).includes(true)) {
  //                     walk.stop();
  //                 }
  //             }
  //             if (evt.keyCode == 87) {
  //                 //W
  //                 keys.forward = false;
  //                 if (!Object.values(keys).includes(true)) {
  //                     walk.stop();
  //                 }
  //             }
  //             if (evt.keyCode == 83) {
  //                 //S
  //                 keys.back = false;
  //                 if (!Object.values(keys).includes(true)) {
  //                     walk.stop();
  //                 }
  //             }
  //             if (evt.keyCode == 83) {
  //                 //S
  //                 keys.reload = false;
  //                 if (!Object.values(keys).includes(true)) {
  //                     walk.stop();
  //                 }
  //             }
  //         }

  // engine.runRenderLoop(function () {
  //     if (fpsMesh != null) {
  //         fpsMesh.update();
  //     }
  // });
  //     }
  // );

  //UI
  // add crosshair circle
  var advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("Ui");
  var crosschair = GUI.Button.CreateImageOnlyButton(
    "b1",
    "https://dl.dropbox.com/s/q37l562ms3u9sd6/circle.svg"
  )!;
  crosschair.image!.stretch = GUI.Image.STRETCH_UNIFORM;
  crosschair.width = "24px";
  crosschair.height = "24px";
  crosschair.color = "transparent";
  advancedTexture.addControl(crosschair);

  //         var utilLayer = new BABYLON.UtilityLayerRenderer(scene);
  //         var gizmo1 = new BABYLON.PositionGizmo(utilLayer);
  //         var gizmo2 = new BABYLON.PositionGizmo(utilLayer);
  //         var gizmo3 = new BABYLON.PositionGizmo(utilLayer);

  //         // Gizmo to camera
  //         gizmo1.attachedMesh = fpsCamera;
  //         // gizmo2.attachedMesh = fpsMesh;

  //         tpsCamera.minZ = 0.0001;
  //         fpsMesh.rotation = player.rotation;

  engine.hideLoadingUI();
  return scene;
};

export default createScene;
