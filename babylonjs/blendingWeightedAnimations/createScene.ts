// this lesson shows how animations can be
// merged by adjusting their weights
// it's great for animataion transitionings
// almost like a fade-in, fade-out of an animation
// like 'fading-in' to running

import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";

import makeTextPlane from "../shapesDemo/makeTextPlane";

function createGround(scene: BABYLON.Scene) {
  // GROUND
  // Ground for positional reference
  const ground = BABYLON.MeshBuilder.CreateGround("ground", {
    width: 60,
    height: 60,
  });
  ground.material = new GridMaterial("groundMat", scene);
  ground.material.backFaceCulling = false;
}

function createCamera(scene: BABYLON.Scene) {
  const camera = new BABYLON.ArcRotateCamera(
    "camera1",
    Math.PI / 2,
    Math.PI / 4,
    3,
    new BABYLON.Vector3(0, 1, 0),
    scene
  );
  camera.attachControl(true);
  camera.lowerRadiusLimit = 2;
  // camera.upperRadiusLimit = 10;
  camera.wheelDeltaPercentage = 0.01;
}

function createHemisphericLight(scene: BABYLON.Scene) {
  const light = new BABYLON.HemisphericLight(
    "light1",
    new BABYLON.Vector3(0, 1, 0),
    scene
  );
  light.intensity = 0.6;
  light.specular = BABYLON.Color3.Black();
}

function createDirectionalLight(scene: BABYLON.Scene) {
  const light2 = new BABYLON.DirectionalLight(
    "dir01",
    new BABYLON.Vector3(0, -0.5, -1.0),
    scene
  );
  light2.position = new BABYLON.Vector3(0, 5, 5);
  return light2;
}

async function createScene(engine: BABYLON.Engine) {
  var scene = new BABYLON.Scene(engine);
  createCamera(scene);
  createHemisphericLight(scene);
  const light2 = createDirectionalLight(scene);

  // TODO: learn about skybox, ground
  var helper = scene.createDefaultEnvironment({
    enableGroundShadow: true,
  })!;
  helper.setMainColor(BABYLON.Color3.Gray());
  helper.ground!.position.y += 0.01;

  // ????????
  engine.enableOfflineSupport = false;
  BABYLON.Animation.AllowMatricesInterpolation = true;

  // Force the babylon loader screen to appear
  engine.displayLoadingUI();

  // IMPORT DUMMY MODEL
  const { meshes, skeletons, particleSystems } =
    await BABYLON.SceneLoader.ImportMeshAsync(
      "",
      "./",
      "dummy2.babylon",
      scene
    );

  // ADD DUMMY SHADOW
  var shadowGenerator = new BABYLON.ShadowGenerator(1024, light2);
  shadowGenerator.useBlurExponentialShadowMap = true;
  shadowGenerator.blurKernel = 32;
  shadowGenerator.addShadowCaster(meshes[0], true);
  meshes.forEach((mesh) => (mesh.receiveShadows = false));

  // Animations
  const skeleton = skeletons[0];

  // begin animation baked into the skeleton
  // froms 0-89 are the 'idle' frames
  // set weight to 1
  const idleAnim = scene.beginWeightedAnimation(
    skeleton, // target
    0, // from
    89, // to
    1, // weight
    true // loop?
  );

  // 90-118 are the 'walk' frames
  // set weight to 0
  const walkAnim = scene.beginWeightedAnimation(skeleton, 90, 118, 0, true);

  // 119-135 are the 'run' frames
  // set weight to 0
  const runAnim = scene.beginWeightedAnimation(skeleton, 119, 135, 0, true);

  // UI
  const advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
  const UiPanel = new GUI.StackPanel();
  UiPanel.width = "220px";
  UiPanel.fontSize = "14px";
  UiPanel.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
  UiPanel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_CENTER;
  UiPanel.background = "blue";
  advancedTexture.addControl(UiPanel);

  interface Behavior {
    name: "Idle" | "Walk" | "Run";
    anim: BABYLON.Animatable;
    slider: GUI.Slider;
  }

  const idleBehavior: Behavior = {
    name: "Idle",
    anim: idleAnim,
    slider: new GUI.Slider(),
  };
  const walkBehavior: Behavior = {
    name: "Walk",
    anim: walkAnim,
    slider: new GUI.Slider(),
  };
  const runBehavior: Behavior = {
    name: "Run",
    anim: runAnim,
    slider: new GUI.Slider(),
  };

  // SLIDERS
  [idleBehavior, walkBehavior, runBehavior].forEach((behavior) => {
    // LABEL TEXT
    var header = new GUI.TextBlock();
    header.text = behavior.name + ":" + behavior.anim.weight.toFixed(2);
    header.height = "40px";
    header.color = "green";
    header.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    header.paddingTop = "10px";
    UiPanel.addControl(header);

    // SLIDER CONTROL
    const slider = behavior.slider;
    slider.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    slider.minimum = 0;
    slider.maximum = 1;
    slider.color = "green";
    slider.value = behavior.anim.weight;
    slider.height = "20px";
    slider.width = "205px";
    UiPanel.addControl(slider);

    // slider onchange event
    slider.onValueChangedObservable.add((v) => {
      behavior.anim.weight = v;
      header.text = behavior.name + ":" + behavior.anim.weight.toFixed(2);
    });
    behavior.slider = slider;
  });

  // BUTTON: from idle to walk
  const idleToWalkButton = GUI.Button.CreateSimpleButton(
    "idleToWalkButton",
    "From idle to walk"
  );
  idleToWalkButton.paddingTop = "10px"; // padding seems to be like margin
  idleToWalkButton.width = "100px";
  idleToWalkButton.height = "50px";
  idleToWalkButton.color = "white";
  idleToWalkButton.background = "green";
  UiPanel.addControl(idleToWalkButton);

  idleToWalkButton.onPointerDownObservable.add(function () {
    idleBehavior.slider.value = 1.0;
    walkBehavior.slider.value = 0;
    runBehavior.slider.value = 0.0;

    // ??????????
    // Synchronize animations
    // runAnim.syncWith(null);
    // idleAnim.syncWith(walkAnim);
    const observable = scene.onBeforeAnimationsObservable.add(function () {
      idleBehavior.slider.value -= 0.01;
      if (idleBehavior.slider.value <= 0) {
        scene.onBeforeAnimationsObservable.remove(observable);
        idleBehavior.slider.value = 0;
        walkBehavior.slider.value = 1.0;
      } else {
        walkBehavior.slider.value = 1.0 - idleBehavior.slider.value;
      }
    });
  });

  // BUTTON: from walk to run
  const walkToRunButton = GUI.Button.CreateSimpleButton(
    "walkToRunButton",
    "From walk to run"
  );
  walkToRunButton.paddingTop = "10px"; // padding seems to be like margin
  walkToRunButton.width = "100px";
  walkToRunButton.height = "50px";
  walkToRunButton.color = "white";
  walkToRunButton.background = "green";
  UiPanel.addControl(walkToRunButton);

  walkToRunButton.onPointerDownObservable.add(function () {
    idleBehavior.slider.value = 0;
    walkBehavior.slider.value = 1.0;
    runBehavior.slider.value = 0.0;
    // Synchronize animations
    // idleAnim.syncWith(null);
    // walkAnim.syncWith(runAnim);
    const observable = scene.onBeforeAnimationsObservable.add(function () {
      walkBehavior.slider.value -= 0.01;
      if (walkBehavior.slider.value <= 0) {
        scene.onBeforeAnimationsObservable.remove(observable);
        walkBehavior.slider.value = 0;
        runBehavior.slider.value = 1.0;
      } else {
        runBehavior.slider.value = 1.0 - walkBehavior.slider.value;
      }
    });
  });

  // BUTTON: from run to walk
  const runToWalkButton = GUI.Button.CreateSimpleButton(
    "runToWalkButton",
    "From run to walk"
  );
  runToWalkButton.paddingTop = "10px"; // padding seems to be like margin
  runToWalkButton.width = "100px";
  runToWalkButton.height = "50px";
  runToWalkButton.color = "white";
  runToWalkButton.background = "green";
  UiPanel.addControl(runToWalkButton);

  runToWalkButton.onPointerDownObservable.add(function () {
    idleBehavior.slider.value = 0;
    walkBehavior.slider.value = 0;
    runBehavior.slider.value = 1.0;
    // Synchronize animations

    // Synchronize and normalize current Animatable with
    // a source Animatable This is useful when using
    // animation weights and when animations are not of
    // the same length
    // idleAnim.syncWith(null);
    // runAnim.syncWith(walkAnim);
    const observable = scene.onBeforeAnimationsObservable.add(function () {
      runBehavior.slider.value -= 0.01;
      if (runBehavior.slider.value <= 0) {
        scene.onBeforeAnimationsObservable.remove(observable);
        runBehavior.slider.value = 0;
        walkBehavior.slider.value = 1.0;
      } else {
        walkBehavior.slider.value = 1.0 - runBehavior.slider.value;
      }
    });
  });

  // BUTTON: from walk to idle
  const walkToIdleButton = GUI.Button.CreateSimpleButton(
    "walkToIdleButton",
    "From walk to idle"
  );
  walkToIdleButton.paddingTop = "10px"; // padding seems to be like margin
  walkToIdleButton.width = "100px";
  walkToIdleButton.height = "50px";
  walkToIdleButton.color = "white";
  walkToIdleButton.background = "green";
  UiPanel.addControl(walkToIdleButton);

  walkToIdleButton.onPointerDownObservable.add(function () {
    idleBehavior.slider.value = 0;
    walkBehavior.slider.value = 1.0;
    runBehavior.slider.value = 0;
    // Synchronize animations

    // Synchronize and normalize current Animatable with
    // a source Animatable This is useful when using
    // animation weights and when animations are not of
    // the same length
    // idleAnim.syncWith(null);
    // runAnim.syncWith(walkAnim);
    const observable = scene.onBeforeAnimationsObservable.add(function () {
      walkBehavior.slider.value -= 0.01;
      if (walkBehavior.slider.value <= 0) {
        scene.onBeforeAnimationsObservable.remove(observable);
        walkBehavior.slider.value = 0;
        idleBehavior.slider.value = 1.0;
      } else {
        idleBehavior.slider.value = 1.0 - walkBehavior.slider.value;
      }
    });
  });

  engine.hideLoadingUI();

  return scene;
}

export default createScene;
