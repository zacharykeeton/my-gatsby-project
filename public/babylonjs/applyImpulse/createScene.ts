import * as BABYLON from "@babylonjs/core";
import { GridMaterial } from "@babylonjs/materials/grid";
import * as GUI from "@babylonjs/gui/2D";
import "@babylonjs/loaders/glTF";
import { PhysicsEngine, HavokPlugin } from "@babylonjs/core/Physics";
import HavokPhysics from "@babylonjs/havok";

import makeTextPlane from "../shapesDemo/makeTextPlane";
import { SceneExplorerComponent } from "@babylonjs/inspector/components/sceneExplorer/sceneExplorerComponent";

async function createScene(engine: BABYLON.Engine) {
    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);

    // This creates and positions a free camera (non-mesh)
    var camera = new BABYLON.FreeCamera(
        "camera1",
        new BABYLON.Vector3(0, 5, -25),
        scene
    );

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    var light = new BABYLON.HemisphericLight(
        "light",
        new BABYLON.Vector3(0, 1, 0),
        scene
    );

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Our built-in 'sphere' shape.
    var sphere = BABYLON.MeshBuilder.CreateSphere(
        "sphere",
        { diameter: 2, segments: 32 },
        scene
    );

    // Move the sphere upward at 4 units
    sphere.position.y = 4;
    sphere.position.x = -4;

    // Our built-in 'ground' shape.
    var ground = BABYLON.MeshBuilder.CreateGround(
        "ground",
        { width: 20, height: 10 },
        scene
    );

    // initialize plugin
    const havokInterface = await HavokPhysics();
    const hk = new HavokPlugin(undefined, havokInterface);
    // enable physics in the scene with a gravity
    scene.enablePhysics(new BABYLON.Vector3(0, -9.8, 0), hk);

    // Create a sphere shape and the associated body. Size will be determined automatically.
    var sphereAggregate = new BABYLON.PhysicsAggregate(
        sphere,
        BABYLON.PhysicsShapeType.SPHERE,
        { mass: 1, restitution: 0.75 },
        scene
    );

    // Create a static box shape.
    var groundAggregate = new BABYLON.PhysicsAggregate(
        ground,
        BABYLON.PhysicsShapeType.BOX,
        { mass: 0 },
        scene
    );

    function createBox(name: string, position: BABYLON.Vector3) {
        const box = BABYLON.MeshBuilder.CreateBox(name, { height: 3 });
        box.position = position;
        box.visibility = 0.2;
        var boxAggregate = new BABYLON.PhysicsAggregate(
            box,
            BABYLON.PhysicsShapeType.BOX,
            {
                mass: 1,
                restitution: 0.75,
                // center: new BABYLON.Vector3(0, 0, 0),
            },
            scene
        );

        console.log(boxAggregate.body.getMassProperties());

        const centerOfMass = new BABYLON.Vector3(0, -0.75, 0);
        const centerOfMassIndicator = BABYLON.MeshBuilder.CreateSphere(
            "centerOfMassIndicator",
            { diameter: 0.2 }
        );
        centerOfMassIndicator.position = centerOfMass;
        centerOfMassIndicator.parent = box;
        centerOfMassIndicator.material = new BABYLON.StandardMaterial(
            "centerOfMassMaterial"
        );
        (
            centerOfMassIndicator.material as BABYLON.StandardMaterial
        ).diffuseColor = new BABYLON.Color3(1, 0, 0);

        boxAggregate.body.setMassProperties({
            mass: 1,
            centerOfMass,
        });
        // console.log("NEW", boxAggregate.body.getMassProperties());

        // player is a PhysicsBody
        boxAggregate.body.setCollisionCallbackEnabled(true);

        // You have two options:
        // Body-specific callback
        const observable = boxAggregate.body.getCollisionObservable();
        const observer = observable.add((collisionEvent) => {
            console.log(
                collisionEvent.collider.transformNode.name,
                collisionEvent.collidedAgainst
            );
            if (
                collisionEvent.collidedAgainst.transformNode.name === "sphere"
            ) {
                // Dispose of the sphere
                collisionEvent.collidedAgainst.shape?.dispose();
                collisionEvent.collidedAgainst.transformNode.dispose();
                collisionEvent.collidedAgainst.dispose();
            }
        });
    }

    createBox("box0", new BABYLON.Vector3(3, 1.5, 0));
    createBox("box1", new BABYLON.Vector3(4.5, 1.5, 0.75));
    createBox("box2", new BABYLON.Vector3(4.5, 1.5, -0.75));

    //When clicking the display, sphere will jump.
    // scene.onPointerDown = function () {
    //     sphereAggregate.body.applyImpulse(
    //         new BABYLON.Vector3(3, 8, 0),
    //         sphere.absolutePosition
    //     );
    // };

    sphere.actionManager = new BABYLON.ActionManager(scene);
    sphere.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(
            BABYLON.ActionManager.OnPickTrigger,
            function () {
                sphereAggregate.body.applyImpulse(
                    new BABYLON.Vector3(8, 4, 0),
                    sphere.absolutePosition
                );
            }
        )
    );

    return scene;
}

export default createScene;
