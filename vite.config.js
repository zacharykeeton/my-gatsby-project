import { resolve } from "path";
import { defineConfig } from "vite";

export default defineConfig(({ command, mode }) => {
  return {
    assetsInclude: ["**/*.glb", "**/*.babylon"],
    resolve: {
      alias: {
        babylonjs:
          mode === "development" ? "babylonjs/babylon.max" : "babylonjs",
      },
    },
    build: {
      target: "esnext", // you can also use 'es2020' here
      rollupOptions: {
        input: {
          main: resolve(__dirname, "index.html"),
          devTeamLeaderShipTactics: resolve(
            __dirname,
            "dev-team-leadership-tactics.html"
          ),
          game1: resolve(__dirname, "babylonjs/game1/index2.html"),
          shapesDemo: resolve(__dirname, "babylonjs/shapesDemo/index.html"),
          winterScene: resolve(__dirname, "babylonjs/winterScene/index.html"),
          particlesLab: resolve(__dirname, "babylonjs/particlesLab/index.html"),
          animationLab: resolve(__dirname, "babylonjs/animationLab/index.html"),
          animationLab2: resolve(
            __dirname,
            "babylonjs/animationLab2/index.html"
          ),
          animationLab3: resolve(
            __dirname,
            "babylonjs/groupingAnimationsLab/index.html"
          ),
          animationLab4: resolve(
            __dirname,
            "babylonjs/blendingWeightedAnimations/index.html"
          ),
          animationLab5: resolve(
            __dirname,
            "babylonjs/additiveAnimations/index.html"
          ),
          polygonGame: resolve(__dirname, "babylonjs/polygonGame/index.html"),
          physicsSetup: resolve(__dirname, "babylonjs/physicsSetup/index.html"),
          physicsSleep: resolve(__dirname, "babylonjs/physicsSleep/index.html"),
          applyImpulse: resolve(__dirname, "babylonjs/applyImpulse/index.html"),
          babylon101FPS: resolve(
            __dirname,
            "babylonjs/babylon101FPS/index.html"
          ),
          additiveAnimationsCoins: resolve(
            __dirname,
            "babylonjs/additiveAnimationsCoins/index.html"
          ),
          fpsStarter: resolve(__dirname, "babylonjs/fpsStarter/index.html"),
          phaserDemo: resolve(__dirname, "phaserio/demo/index.html"),
          zowie: resolve(__dirname, "phaserio/zowie/index.html"),
          multiScene: resolve(__dirname, "phaserio/multiscene/index.html"),
          claw: resolve(__dirname, "blender/claw/index.html"),
          http: resolve(__dirname, "blender/http/index.html"),
          reduce: resolve(__dirname, "blender/reduce/index.html"),
        },
      },
    },
    optimizeDeps: {
      esbuildOptions: {
        target: "esnext", // you can also use 'es2020' here
      },
    },
  };
});
