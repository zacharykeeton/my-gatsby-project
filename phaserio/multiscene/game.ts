import { Game } from "phaser";
import config from "./config";

const game = new Game({
  ...config,
  parent: document.querySelector("#gameContainer") as HTMLDivElement,
});
