import { Types } from "phaser";
import * as Phaser from "phaser";
import WelcomeScene from "./scenes/Welcome";
import GameScene from "./scenes/Game";

var config: Types.Core.GameConfig = {
  type: Phaser.AUTO,
  width: 500,
  height: 500,
  scale: {
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  input: {
    gamepad: true,
  },
  physics: {
    default: "arcade",
    arcade: {
      debug: false,
    },
  },
  scene: [WelcomeScene, GameScene],
};

export default config;
