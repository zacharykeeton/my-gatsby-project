import { Scene } from "phaser";
import Scenes from "./Scenes";

export default class GameScene extends Scene {
  constructor() {
    super("game"); // set scene name
  }
  preload() {}

  create() {
    // GameScreen Text to serve as identifier
    this.add.text(100, 100, "GameScreen", {
      fontSize: "20px",
    });

    this.add.text(100, 300, "Press E to exit", {
      fontSize: "20px",
    });

    this.input.keyboard?.on("keydown-E", () => {
      this.scene.start(Scenes.Welcome); // start new scene
      this.scene.stop();
    });
  }

  update() {}
}
