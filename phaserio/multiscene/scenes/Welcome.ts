import Phaser, { Scene } from "phaser";

import Scenes from "./Scenes";

function pressEnterToPlay(scene: Phaser.Scene) {
  scene.add.text(100, 300, "Press Enter to play", {
    fontSize: "20px",
  });

  scene.input.keyboard?.on("keydown-ENTER", () => {
    scene.scene.pause();
    console.log("ENTER");
    //   this.song?.pause();
    scene.scene.start(Scenes.Game); // start new scene
  });
}

export default class WelcomeScene extends Scene {
  song: Phaser.Sound.WebAudioSound | null = null;
  private numText: Phaser.GameObjects.Text | null = null;

  constructor() {
    super(Scenes.Welcome); // set scene name
  }
  preload() {
    this.load.audio("bgMusic", "./assets/backgroundMusic.mp3", {
      volume: 0.5,
    });
  }

  create() {
    // WelcomeScreen Text to serve as identifier
    this.add.text(100, 100, "WelcomeScreen", {
      fontSize: "20px",
    });
    pressEnterToPlay(this);

    this.add.text(100, 300, "Press Enter to play", {
      fontSize: "20px",
    });

    this.numText = this.add.text(100, 450, "0", {
      fontSize: "20px",
    });

    // text.text = "zzz";

    // ADD SONG
    // this.song = this.sound.add("bgMusic", {
    //   volume: 0.5,
    //   loop: true,
    // }) as Phaser.Sound.WebAudioSound;
    // // this.song.play();

    // this.input.keyboard.on("keydown-ENTER", () => {
    //   this.scene.pause();
    //   console.log("ENTER");
    //   //   this.song?.pause();
    //   //   this.scene.pause('welcome')
    //   this.scene.start(Scenes.Game); // start new scene
    // });

    this.input.keyboard?.on("keydown-E", () => {
      console.log("E");
      this.scene.resume();

      //   this.scene.start(Scenes.Welcome);
      //   this.song?.pause();
      //   this.scene.pause('welcome')
      //   this.scene.start(Scenes.Game); // start new scene
    });
  }

  //   update() {
  //     if (!this.song?.isPaused) {
  //       this.song?.resume();
  //     }
  //   }

  private counter = 0;
  update(time: number, delta: number) {
    this.numText && (this.numText.text = (this.counter++).toString());
  }
}
