import { Types, AUTO, Scale } from "phaser";
import GameScene from "./scene";

const config: Types.Core.GameConfig = {
  type: AUTO,
  width: 800,
  height: 600,
  scale: {
    autoCenter: Scale.CENTER_BOTH,
  },
  input: {
    gamepad: true,
  },
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 300 },
      debug: false,
    },
  },
  scene: [GameScene],
};

export default config;
