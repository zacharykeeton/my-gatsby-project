import { Scene, Physics, Types, Tilemaps } from "phaser";
import * as Phaser from "phaser";
// import skyUrl from

export default class GameScene extends Scene {
  // platforms: Physics.Arcade.StaticGroup | undefined;
  player: Physics.Arcade.Sprite | undefined;
  cursors: Types.Input.Keyboard.CursorKeys | undefined;
  // stars: Physics.Arcade.Group | undefined;
  // score = 0;
  // scoreText: Phaser.GameObjects.Text | undefined;
  // bombs: Physics.Arcade.Group | undefined;
  gameOver = false;

  constructor() {
    super("game"); // set scene name
  }
  preload() {
    /*
      Let's load the assets we need for our game. You do this 
      by putting calls to the Phaser Loader inside of a Scene 
      function called preload. Phaser will automatically look 
      for this function when it starts and load anything 
      defined within it.
    */

    // See local /assets folder
    this.load.image("sky", "/phaserio/zowie/sky.png");
    this.load.image("ground", "/phaserio/zowie/platform.png");
    this.load.image("star", "/phaserio/zowie/strawberry_small.png");
    this.load.image("bomb", "/phaserio/zowie/bomb.png");
    this.load.spritesheet("dude", "/phaserio/zowie/dude2.png", {
      frameWidth: 32,
      frameHeight: 48,
    });
  }

  create() {
    // Why 400 and 300? It's because in Phaser 3
    // all Game Objects are positioned based on
    // their center by default.

    // Hint: You can use setOrigin to change this.
    // For example the code: this.add.image(0, 0, 'sky').setOrigin(0, 0)
    this.add.image(400, 300, "sky");

    // The order in which game objects are displayed matches the order
    // in which you create them.
    // this.add.image(400, 300, "star");

    // All Game Objects created by or added to this Group will
    // automatically be given static Arcade Physics bodies,
    // if they have no body
    // In Arcade Physics there are two types of physics bodies:
    // Dynamic and Static
    // You can group together similar objects and control them all as
    // one single unit. You can also check for collision between
    // Groups and other game objects
    const platforms = this.physics.add.staticGroup();

    // .create: helper function to create game objects
    // here, they will be physics-enabled children
    // No need for .refreshBody since we are not
    // scaling them
    platforms.create(600, 400, "ground");
    platforms.create(50, 250, "ground");
    platforms.create(750, 220, "ground");

    // the ground-ground
    platforms
      .create(400, 568, "ground")
      // scale it up so that it is full width
      .setScale(2)
      // since you scaled a static physics body
      // update the engine
      .refreshBody();

    // PLAYER
    // Create new game object, sprite, with position 100x450
    // will be dynamic by default
    this.player = this.physics.add.sprite(100, 450, "dude");

    // Bounce slightly after colliding
    this.player.setBounce(0.2);

    // Collide with world bounds (don't allow to leave
    // edge of screen)
    this.player.setCollideWorldBounds(true);

    // This allows you to define a single animation once and apply it to as
    // many Game Objects as you require.
    this.anims.create({
      key: "left",
      frames: this.anims.generateFrameNumbers("dude", { start: 0, end: 3 }),
      frameRate: 10,
      repeat: -1, // loop
    });

    this.anims.create({
      key: "turn",
      frames: [{ key: "dude", frame: 4 }],
      frameRate: 20, // why 20 here?
    });

    this.anims.create({
      key: "right",
      frames: this.anims.generateFrameNumbers("dude", { start: 5, end: 8 }),
      frameRate: 10,
      repeat: -1, // loop
    });

    this.physics.add.collider(this.player, platforms);

    this.cursors = this.input.keyboard?.createCursorKeys();

    // Dynamic group
    // Groups are able to take configuration objects to
    // aid in their setup
    const stars = this.physics.add.group({
      key: "star", // sets the texture key to be the star image
      repeat: 11, // Because it creates 1 child automatically, repeating 11 times means we'll get 12 in total
      setXY: { x: 12, y: 0, stepX: 70 },
    });

    stars.children.iterate(function (child) {
      (child as unknown as Physics.Arcade.Body).setBounceY(
        Phaser.Math.FloatBetween(0.4, 0.8)
      );
      return true;
    });

    // stars vs platforms
    this.physics.add.collider(stars, platforms);

    // stars vs player
    let score = 0;
    const scoreText = this.add.text(16, 16, "score: 0", {
      fontSize: "32px",
      color: "#000",
    });

    const collectStar = (
      player: Types.Physics.Arcade.GameObjectWithBody | Tilemaps.Tile,
      star: Types.Physics.Arcade.GameObjectWithBody | Tilemaps.Tile
    ) => {
      (star as Physics.Arcade.Image).disableBody(true, true);
      score += 10;
      scoreText?.setText(`Score: ${score}`);

      if (stars?.countActive(true) === 0) {
        stars?.children.iterate(function (child) {
          (child as Physics.Arcade.Image).enableBody(
            true,
            (child as Physics.Arcade.Image).x,
            0,
            true,
            true
          );
          return true;
        });

        // First we pick a random x coordinate for it,
        // always on the opposite side of the screen to the
        // player, just to give them a chance.
        const newBombX =
          (player as Physics.Arcade.Sprite).x < 400
            ? Phaser.Math.Between(400, 800)
            : Phaser.Math.Between(0, 400);

        const bomb = bombs?.create(newBombX, 16, "bomb");
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
      }
    };

    this.physics.add.overlap(this.player, stars, collectStar, undefined, this);

    const hitBomb = (
      player: Types.Physics.Arcade.GameObjectWithBody | Tilemaps.Tile,
      bomb: Types.Physics.Arcade.GameObjectWithBody | Tilemaps.Tile
    ) => {
      this.physics.pause();

      (player as Physics.Arcade.Sprite).setTint(0xff0000);
      (player as Physics.Arcade.Sprite).anims.play("turn");

      this.gameOver = true;
    };
    const bombs = this.physics.add.group();
    this.physics.add.collider(bombs, platforms);
    this.physics.add.collider(this.player, bombs, hitBomb, undefined, this);
  }

  update() {
    // LEFT
    if (this.cursors?.left.isDown) {
      this.player?.setVelocityX(-160); //px/sec sq
      this.player?.anims.play("left", true);

      // RIGHT
    } else if (this.cursors?.right.isDown) {
      this.player?.setVelocityX(160);
      this.player?.anims.play("right", true);

      // NONE
    } else {
      this.player?.setVelocityX(0);
      this.player?.anims.play("turn");
    }

    // JUMP
    if (this.cursors?.up.isDown && this.player?.body?.touching.down) {
      this.player?.setVelocityY(-330);
    }
  }
}
