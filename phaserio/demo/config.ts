import { Types, AUTO, Scale } from "phaser";
import GameScene from "./scene";

const config: Types.Core.GameConfig = {
  type: AUTO,
  width: 500,
  height: 500,
  scale: {
    autoCenter: Scale.CENTER_BOTH,
  },
  input: {
    gamepad: true,
  },
  physics: {
    default: "arcade",
    arcade: {
      debug: false,
    },
  },
  scene: [GameScene],
};

export default config;
